﻿using UnityEngine;
using System.Collections;

public class ParralaxEffect : MonoBehaviour {
	public float distance = 0.0f;
	public GameObject accroche;
	float timeCurrent = 0.0f;
	float delay = 1.0f;
	// Use this for initialization
	void Start () {
		timeCurrent = Time.fixedTime;
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.position =  new Vector3 (transform.position.x-distance, transform.position.y, transform.position.z);
		if (timeCurrent + delay < Time.fixedTime)
				{
					transform.position = new Vector3 (accroche.transform.position.x, accroche.transform.position.y, accroche.transform.position.z);
					timeCurrent = Time.fixedTime;
				}
	}
	

}
