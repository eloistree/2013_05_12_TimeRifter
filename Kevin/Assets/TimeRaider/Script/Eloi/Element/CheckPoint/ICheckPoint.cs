﻿using UnityEngine;
using System.Collections;

public interface ICheckPoint  {
	
	void AddListener(ICheckPointListener listener);
	void ClearListener();
	void SetState (CheckPointState value);
	Vector2 GetRepopPosition();
}

public enum CheckPointState
{
	Hide, Enable, Checked
}
