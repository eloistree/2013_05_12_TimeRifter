﻿using UnityEngine;
using System.Collections;

public class PlayerAnimFabImpl : PlayerImpl, Refreshable
{
	public Refresher.RefreshType refreshType= Refresher.RefreshType.Quick;
	public Animator bodyEngine;

	public float height;
	public float MaxWalkingSpeed=8f;
	public bool isFalling;
	public bool isJumping;
	
	public bool isWaking;
	public bool isRunning;

	
	public bool isSliding;

	public float groundHeightDestect=0.75f;




	public void Refresh (float time)
	{
		//set the height of the player
		Vector2 o = this.gameObject.transform.position;
		o.x+=groundHeightDestect;
		Vector2 d = -Vector2.up;

		RaycastHit2D [] hit = Physics2D.LinecastAll (o,d,1000);
		Debug.DrawRay(o,d,Color.red,10f);
		if(hit!=null){
			float maxheight=0;
			foreach(RaycastHit2D h in hit){
			float hh = o.y-h.point.y;
				if( hh>maxheight) maxheight=hh;
			}
			height = maxheight;	//		print ("...  "+height);
		}
		else height=0;
		if (bodyEngine != null) {
			
			//Define the last height value
			bodyEngine.SetFloat("Height",height);
		}

		//Is the player is jumping or falling
		Rigidbody2D r = this.gameObject.rigidbody2D;
		if(r!=null)
		{
			if(r.velocity.y>0.1)isJumping=true;
			else if(r.velocity.y<-0.1)isFalling=true;
		}

		///Set speed and state of running
		SetStateOnSpeed(speedInfo);

	}

	public void SetStateOnSpeed(float speed)
	{
		if(speed==0){ isWaking=false; isRunning=false;}
		else if( speed<MaxWalkingSpeed)
		{
			isWaking=true; isRunning=false;
		}
		else {
			isWaking=false; isRunning=true;
		}

	}

	public Refresher.RefreshType GetRefreshType ()
	{return refreshType;}
	
	public override void SetState (PlayerState value)
	{
		base.SetState (value);
	
		isSliding = PlayerState.Sliding.Equals(value);


		if (bodyEngine != null) {

			//Define the last height value
			bodyEngine.SetFloat("Height",height);
			bodyEngine.SetBool("IsJump",isJumping);
			bodyEngine.SetBool("IsFall",isFalling);
			bodyEngine.SetBool("IsSlipping",isSliding);

			switch (value) {
			case PlayerState.Death:
				bodyEngine.SetInteger("Life",0);
				break;		

			case PlayerState.JumpingBack:
				bodyEngine.SetTrigger ("KnockBack");
			break;		
			case PlayerState.Jumping:
				bodyEngine.SetTrigger ("Jump");
				break;	
			
//			case PlayerState.Running:
//				speed =20f;break;
//			case PlayerState.NotMoving:
//			case PlayerState.Waiting:
//				speed=0f;
//				break;
			}
//			SetStateOnSpeed();
//			bodyEngine.SetBool("IsWalk",isWaking);
//			bodyEngine.SetBool("IsRunning",isRunning);
		
		}
	}
}