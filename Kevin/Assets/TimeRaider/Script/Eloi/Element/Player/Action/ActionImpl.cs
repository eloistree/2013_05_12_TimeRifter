﻿using UnityEngine;
using System.Collections;

public class ActionImpl : Action {
	
	public GameObject teleportInPref;
	public GameObject teleportOutPref;


	public void Update()
	{
		}




	protected override  void ActionStartApply (Vector2 direction, Transform obj, Actions action)
	{
		switch(action)
		{
		case Actions.Teleport:	
			if(teleportInPref!=null && entityLocalisation!=null)
				GameObject.Instantiate(teleportInPref,  obj.position, obj.rotation);
			break;
			
		}

	}

	protected override void ActionEndApply (Vector2 direction, Transform obj, Actions action)
	{
		switch(action)
		{
			case Actions.Teleport:
			if(teleportOutPref!=null && entityLocalisation!=null)
				GameObject.Instantiate(teleportOutPref,  obj.position, obj.rotation);
			break;
			
			
		}
	}

}
