﻿using UnityEngine;
using System.Collections;


public interface I_Action  {

	  /** Get and setter of the direction and the intensity of an action	*/
	  void 	SetAction 		(Vector2 direction, float multiplicatorRatio,  Actions action);

	  /** Do the action with the default direction setted					*/
	  void  DoThisAction 	(Actions action);

	  /**Do the action in the direction given	 							*/
	  void 	DoThisAction 	(Vector2 direction, Actions action);


}

public enum Actions {Jump, Slide, Dash, JumpBack, Teleport, GroundDash,None}
