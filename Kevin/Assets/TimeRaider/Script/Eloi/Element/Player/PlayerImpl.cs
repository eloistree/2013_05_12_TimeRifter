﻿using UnityEngine;
using System.Collections;

public class PlayerImpl :Player {
	
	public string groundLayerName= "Ground";
	public Transform groundDetectorPoint;
	public Transform groundCornerDetectorPoint;
	public Transform wallDetectorPoint;
	public Transform mindStatePoint;



	
	public  override Vector2 GetPosition(){
	
		return this.gameObject.transform.position;
	}


	public override void ApplyEffect (Trap.TrapEffect [] trapeffect, GameObject trap)
	{
		base.ApplyEffect(trapeffect,trap);
		foreach(Trap.TrapEffect effect in trapeffect){
		
					switch(effect)
					{
						
						
					case Trap.TrapEffect.Kill: 		break;
					case Trap.TrapEffect.Dommage:	break; 
					case Trap.TrapEffect.Slow: 		break; 
					case Trap.TrapEffect.Fear: 		break;  
					case Trap.TrapEffect.PushOver :
						
						if(this.gameObject.rigidbody2D!=null)
						{
							this.gameObject.rigidbody2D.AddForce(new Vector2(-1000,1000));
						}
						break;  
						case Trap.TrapEffect.Stun: break; 
						
					}
					
			
		}
		
		
		
	}
	
	public override bool IsOverGround ()
	{
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		return Physics2D.Linecast(transform.position, groundDetectorPoint.position, 1<<LayerMask.NameToLayer(groundLayerName));  
	}

	public override bool IsOverGroundToJump ()
	{
		bool ground =Physics2D.Linecast(transform.position, groundDetectorPoint.position, 1<<LayerMask.NameToLayer(groundLayerName)); 
		bool leftGroundCorner = Physics2D.Linecast(transform.position, groundCornerDetectorPoint.position, 1<<LayerMask.NameToLayer(groundLayerName)); ;
		return  ground || leftGroundCorner;

	}

	public override bool IsWallBlock ()
	{
		return Physics2D.Linecast(transform.position, wallDetectorPoint.position, 1<<LayerMask.NameToLayer(groundLayerName));

	}

	public override void SetState (PlayerState value)
	{
		base.SetState (value);
	
	}

	public override void SetMindState (PlayerMindState value)
	{
		base.SetMindState (value);
	}

}
