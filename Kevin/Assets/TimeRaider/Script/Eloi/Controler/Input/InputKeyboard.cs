﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public  abstract class InputKeyboard :InputBasic {


	/**Can be remplace by two table, with keycode and string*/


	public KeyCode jump= 		KeyCode		.UpArrow;
	public KeyCode backjump= 	KeyCode		.LeftArrow;
	public KeyCode dashDown= 	KeyCode		.DownArrow;
	public KeyCode slide= 		KeyCode		.RightArrow;
	public KeyCode teleport= 	KeyCode		.Space;

	
	public KeyCode slowSpeed = KeyCode.S;
	public KeyCode increaseSpeed = KeyCode.D;
	
	public KeyCode pause = KeyCode.P;


	public KeyCode menu= KeyCode.Escape;
	public KeyCode cancel= KeyCode.Backslash;
	public KeyCode valider= KeyCode.Return;
	public KeyCode yes= KeyCode.Y;
	public KeyCode no= KeyCode.N;
/*
	public KeyCode previousMenuElement = KeyCode.LeftArrow;
	public KeyCode nextMenuElement = KeyCode.RightArrow;

*/



	protected void InputUsed(InputPossibility movetype, float time)
	{

		foreach (I_InputListener l in listeners) {
			l.InputEvent(movetype, time);
		}
	}
}
