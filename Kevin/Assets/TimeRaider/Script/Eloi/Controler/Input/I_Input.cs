﻿using UnityEngine;
using System.Collections;

public interface I_Input  {
	
	void AddListener(I_InputListener listener);
	void ClearListener();
}
