﻿using UnityEngine;
using System.Collections;

public class TimeRaiderMobileInput : InputMobile , Refreshable{

	public Refresher.RefreshType refreshType = Refresher.RefreshType.Quick;

	public GameObject 	GObjPowerA;
	public GameObject 	GObjPowerB;
	public GameObject 	GObjBoost;


	public IPowerButton 		powerA;
	public IPowerButton 		powerB;
	public IBoostButton 		boost;

	
	public float doubleClick =0.5f;
	public float slideDistMin =70f;
	
	private float lastPressZoneReleased ;
	// Attention, jump control, should be in INGAME rules and not in the controler !!!
	public float pressZoneReleasedDelay=0.5f ;

	
	private float lastPressBoostZone ;
	// Attention, jump control, should be in INGAME rules and not in the controler !!!
	public float pressBoostZoneDelay=0.5f ;
	
	public float speedMeasureRefresh =0.3f;
	public float lastSpeedMeasureRefresh ;


	private float hasBeenClickRecently = 2f;


	private float lastDoubleClick;
	private float lastSlide;


	void Awake()
	{

	
		boost = GObjBoost.GetComponent(typeof(IBoostButton)) as IBoostButton;
		if(boost==null) throw new UnityException("Boost zone is not define: "+this.gameObject);
		
		powerA = GObjPowerA.GetComponent(typeof(IPowerButton)) as IPowerButton;
		if(powerA==null) throw new UnityException("Power zone A is not define: "+this.gameObject);

		powerB = GObjPowerB.GetComponent(typeof(IPowerButton)) as IPowerButton;
		if(powerB==null) throw new UnityException("Power zone B is not define: "+this.gameObject);


	}

	 public void Refresh(float time)
	{




		MouseLeftPlus mouse = MouseLeftPlus.GetInstance();
		if(mouse!=null)
		{
			//print ("Hi bitches");
			bool zonePressedValide = true;
			float x = mouse.GetLastPressedPosition().x;
			float y = mouse.GetLastPressedPosition().y;

			//if in zone valide
			float lpress = mouse.GetLastReleased();

			if(zonePressedValide 
			   && mouse.WasDoubleClick()
			//   && time-lpress<hasBeenClickRecently
			   && lastDoubleClick < lpress)
			{
				if(zonePressedValide= IsZonePressedValide(x,y))
				{
					lastDoubleClick=lpress;
					//print ("Double click");
					foreach( I_InputListener l in listeners)
					{
						l.InputEvent(InputPossibility.Jump, time);
					}
				}
			}

			lpress = mouse.GetLastPressed();
			if( zonePressedValide 
			   && mouse.IsSliding(slideDistMin) 
			//   && time-lpress<hasBeenClickRecently
			   && lastSlide < lpress)
			{
				if(zonePressedValide= IsZonePressedValide(x,y))
				{
					lastSlide = lpress;
					
					//print ("Slide"+ mouse.GetDirection(true));
					foreach( I_InputListener l in listeners)
					{
						l.InputEvent(InputPossibility.Jump, time, mouse.GetLastDirection(true));
					}
				}
			}

			if( zonePressedValide && time-lastSpeedMeasureRefresh>speedMeasureRefresh){

				if(zonePressedValide= IsZonePressedValide(x,y))
				{

					lastSpeedMeasureRefresh = time;
					
					foreach( I_InputListener l in listeners)
					{
						l.SetMovementDirection(mouse.GetDirectionOnScreen(true));
					}
					
				}
			}
		}
		
	


		if(boost!=null && boost.IsItReady()  && boost.HasBeenClickRecently(hasBeenClickRecently) )
		{


				boost.Activate();

				foreach( I_InputListener l in listeners)
				{
					l.InputEvent(InputPossibility.Teleport, time);
					}

		}


		if(powerA!=null && powerA.IsItReady()  && powerA.HasBeenClickRecently(hasBeenClickRecently) )
		{
			
			
			powerA.Activate();
			
			foreach( I_InputListener l in listeners)
			{
				l.InputEvent(InputPossibility.Slide, time);
			}
			
		}

		
		if(powerB!=null && powerB.IsItReady()  && powerB.HasBeenClickRecently(hasBeenClickRecently) )
		{
			
			
			powerB.Activate();
			
			foreach( I_InputListener l in listeners)
			{
				l.InputEvent(InputPossibility.BackJump, time);
			}
			
		}

		if (Input.GetKeyDown(KeyCode.Escape) ) {
			foreach( I_InputListener l in listeners)
			{
				l.InputEvent(InputPossibility.Menu,time);
			}


		}
	}

	private bool IsZonePressedValide( float x, float y)
	{
		return ! ZoneElement.IsInAZoneNotShared(x,y);
	}

	public Refresher.RefreshType GetRefreshType ()
	{
		return refreshType;
	}
}
