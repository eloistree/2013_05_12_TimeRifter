﻿using UnityEngine;
using System.Collections;

public class InputKeyboardImpl : InputKeyboard {

	
	public void Update(){
		
		float time = Time.timeSinceLevelLoad;
		
		if (Input.GetKeyDown (jump)) {
			InputUsed( InputPossibility.Jump , time );		
		}
		if (Input.GetKeyDown (backjump) ) {
			InputUsed( InputPossibility.BackJump , time );		
		}

		if (Input.GetKeyDown (dashDown) ) {
			InputUsed( InputPossibility.Dash , time );		
		}
		if (Input.GetKeyDown (slide) ) {
			InputUsed( InputPossibility.Slide , time );		
		}
		if (Input.GetKeyDown (teleport) ) {
			InputUsed( InputPossibility.Teleport , time );		
		}
		if (Input.GetKeyDown (slowSpeed) ) {
			InputUsed( InputPossibility.SlowDown , time );		
		}
		if (Input.GetKeyDown (increaseSpeed) ) {
			InputUsed( InputPossibility.SpeedUp , time );		
		}
		if (Input.GetKeyDown (pause) ) {
			InputUsed( InputPossibility.Pause , time );		
		}
		if (Input.GetKeyDown (menu) ) {
			InputUsed( InputPossibility.Menu , time );		
		}
		if (Input.GetKeyDown (cancel) ) {
			InputUsed( InputPossibility.Cancel , time );		
		}
		if (Input.GetKeyDown (yes) ) {
			InputUsed( InputPossibility.Yes , time );		
		}
		if (Input.GetKeyDown (no) ) {
			InputUsed( InputPossibility.No , time );		
		}

	}
}
