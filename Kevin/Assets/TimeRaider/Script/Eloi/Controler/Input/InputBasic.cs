﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class InputBasic :MonoBehaviour, I_Input {

	public LinkedList<I_InputListener> listeners = new LinkedList<I_InputListener> ();
	public void AddListener (I_InputListener listener)
	{
		if(listener!= null)
			listeners.AddFirst (listener);
	}
	
	
	public void ClearListener (){
		listeners.Clear ();
		
	}
	

}
