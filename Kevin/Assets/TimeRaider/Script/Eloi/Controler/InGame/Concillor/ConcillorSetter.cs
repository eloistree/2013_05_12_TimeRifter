﻿using UnityEngine;
using System.Collections;

public class ConcillorSetter  {

	private IGameAbilities game;
	public 	I_Input []  inputs;
	public 	ICheckPoint [] checkPoints;
	public 	I_Player  player;
	
	public	I_CrystalCollector crystalCollector;
	public	I_Rules rules;
	public 	IWindowsMaster  windowMaster;

	public ConcillorSetter(IGameAbilities game )
	{
		if(game==null){
			
			throw new UnityException("The reference to InGame class is required !!!");
		}
		this.game= game;
	}


	public void SetUpTheConcillors()
	{

		// Set up input
		InputConcillor inputConcillor = new InputConcillor(game);
		if(inputs!=null){
			foreach( I_Input input in inputs)
			{
				if(input!=null)
				{
					input.AddListener(inputConcillor);
				}

			}
		}

		PlayerConcillor playerConcillor = new PlayerConcillor( game);

		if(player!=null){
					player.AddListener(playerConcillor);
		}


		InteractifConcillor interConcillor = new InteractifConcillor(game);
		if(crystalCollector!=null)
		{
	
			crystalCollector.AddListener(interConcillor);
		}

		//Idem for the tutorial point
		//Idem for the crystal collector

		RulesConcillor rulesConcillor = new RulesConcillor(game);
		if(rules !=null)
		{
			rules.AddListener (rulesConcillor);
		}


		WindowConcillor windowsConcillor = new WindowConcillor (game);
		if (windowMaster != null) 
		{

			windowMaster.AddListener (windowsConcillor);

		}

	

	}

}
