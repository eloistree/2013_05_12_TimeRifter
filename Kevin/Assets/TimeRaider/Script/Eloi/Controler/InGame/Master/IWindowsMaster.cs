
using System;

public interface IWindowsMaster
{
	void AddListener(I_SubWindowListener listener);
	void ClearListener();

	void DisplayMainMenu();
	void DisplayMenuPause();
	void DisplayOption();
	void DisplayVictory();
	void DisplayDefeat();
}




