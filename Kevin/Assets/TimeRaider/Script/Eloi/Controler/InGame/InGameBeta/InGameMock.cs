using UnityEngine;
using System;

public  abstract class InGameMock : MonoBehaviour, IGameAbilities
{

	protected I_Rules rules;

	protected void Start()
	{

		SetUpConcillors();
		DefineRules();

	}



	/**Create somes concillors that are the zone between the InGame and the independent Element*/
	private void SetUpConcillors()
	{

		IGameAbilities g = (IGameAbilities) this;
		ConcillorSetter cs = new ConcillorSetter( g);

		cs.inputs = 			InGamePartnerInterface.inputs;
		cs.player =  			InGamePartnerInterface.player;
		cs.checkPoints =  		InGamePartnerInterface.checkPoints;
		cs.crystalCollector = 	InGamePartnerInterface.crystalCollector;
		cs.rules = 				InGamePartnerInterface.rules;
		cs.windowMaster = 		InGamePartnerInterface.windowMaster;
		
		
		cs.SetUpTheConcillors();

	}

	private  void DefineRules()
	{
		GameObject [] objs = GameObject.FindObjectsOfType<GameObject>() as GameObject[];
		foreach (GameObject o  in objs){
			I_Rules r= o.GetComponent(typeof(I_Rules)) as I_Rules;
			if (r!=null)
			{
				rules = r;return;
			}

		}
		if(rules==null) throw new UnityException("There is not rules define in this scene");


	}
	
	public abstract void  Do (Actions actionType, float time);
	
	public abstract void  Do (Actions actionType, float time, Vector2 direction);
	
	public  abstract void ChangePlayerSpeed (float power);
	public  abstract void ChangePlayerSpeed (float power, bool normelize);

	public  abstract void ChangePlayerLife (int value, bool decrease);

	public  abstract void AddCrystalsRecolted (int value);
	public  abstract void SetNewCheckPoint (ICheckPoint checkPoint);
	public  abstract void GoTo (InGameToMenu MenuType);

	public  abstract void Display (InGameSubMenu subMenuType);
	public abstract void SetPlayerDeath(bool death);
	public abstract void AffectThePlayerBy (PlayerBonusMalus value);

	public abstract void SetPlayerDirection (Vector2 direction, bool normelized);
	public abstract void SetPause (bool onoff);
	public abstract void Save();
	public abstract void GameWin();
}

