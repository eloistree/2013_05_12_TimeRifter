﻿using UnityEngine;
using System.Collections;

public interface I_InGameData
{
	
	void SetCrystal(int crystal);
	int GetCrystal();
	
	void SaveLastCheckPoint( ICheckPoint cp);
	ICheckPoint GetLastCheckPoint( );
	
	void SetLife(int value);
	int GetLife();


	void SetLastJump(float time);
	void SetLastSlide(float time);
	float GetLastSlide (); 
	float GetLastJump ();
	float GetLastGroundDash ();
	void SetLastGroundDash (float time);
	
	void SetLastJumpBack(float time);
	float GetLastJumpBack ();
	void SetLastDash(float time);
	float GetLastDash ();


	void SetPause(bool onOff);
	bool GetPause();

	void Reset();
	
}