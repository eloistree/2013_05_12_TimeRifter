﻿using UnityEngine;
using System.Collections;

public interface I_RulesListener {

	
	void PlayerOutOfTheMap();
	void PlayerOutOfLife();
	void StopSliding(float time);
}
