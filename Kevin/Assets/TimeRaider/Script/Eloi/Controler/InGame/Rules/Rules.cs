﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Rules : MonoBehaviour, I_Rules {

	protected LinkedList<I_RulesListener> listeners = new LinkedList<I_RulesListener>();
	public void AddListener (I_RulesListener listener)
	{	if(listener!=null)
		{listeners.AddFirst(listener);}}
	public void ClearListener ()
	{listeners.Clear();}

	
	public int startLife =		3;
	public int maxLife =		3;
	public float jumpDelay= 	2f;
	public bool jumpNeedGround= true;
	public float slideDelay=	2f;
	public float dashToGroundDelay=	1f;
	public float jumpBackDelay =2f;
	public float dashDelay =2f;

	public float maxSlideTime = 1.5f;




	
	public int GetStartLife ()						{return startLife;}
	public int GetMaxLife ()						{return maxLife;}
	public float GetJumpDelayBetweenAvailable()		{return jumpDelay;}
	public float GetSlideDelayBetweenAvailable()	{return slideDelay;}

	public bool IsAllowToSlide (float time, float lastslide)
	{
		return time-lastslide>slideDelay;
	}
	public bool IsAllowToJumpBack (float time, float lastjump)
	{
		return time-lastjump>jumpBackDelay;
	}

	public bool IsAllowToDash (float time, float lastDash)
	{
		return time-lastDash>dashDelay;
	}

	public bool IsAllowToJump (float time, float lastjump)
	{
		bool isValide =true;
		if(jumpNeedGround){
			I_Player player  = InGamePartnerInterface.player;
			if(player!=null)
			{
				isValide = 	player.IsOverGroundToJump();
			}
		}
		
		return isValide &&  time-lastjump>jumpDelay;
	}

	public bool IsAllowToDashToTheGround (float time, float lastjump)
	{
		bool isValide =true;

			I_Player player  = InGamePartnerInterface.player;
			if(player!=null)
			{
				isValide =  !	player.IsOverGroundToJump();
			}

		
		return isValide &&  time-lastjump>jumpDelay;
	}

}
