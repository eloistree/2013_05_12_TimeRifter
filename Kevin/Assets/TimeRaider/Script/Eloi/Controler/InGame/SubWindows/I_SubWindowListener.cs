
using System;

public interface I_SubWindowListener
{
	//Signal to the listener that it is on or off to be show
	void IsDisplay(I_SubWindow window, bool onOff);
	
	//Signal to the listener that it has been destroy
	void IsDestroy(I_SubWindow window);

	//Signal that the subwindow button type has been used
	void UseDefaultButton(I_SubWindow window, SubWindowButton  button);

	//Signal to the listener that it ask to switch to a menu section
	void AskToGoAt(I_SubWindow window,SubWindowMenuRequest request);	

	//Signal to the listener that it ask to display the submenu section and ask to be destroy when ready
	void AskToGoAt(I_SubWindow window,SubWindowSubMenuRequest request, bool withDestroy);

}

