﻿using UnityEngine;
using UnityEditor;
using System.Collections;
[CustomEditor(typeof(SubWindowImpl))]
public class SubWindowEditor : Editor {

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		I_SubWindow win = (SubWindow)target;
	
		if (GUILayout.Button ("Destroy")) {
			win.Remove();
		}
	}
}
