﻿using UnityEngine;
using System.Collections;

public class SBE_Restart : SBE_Button {
	
	
	protected void Awake()
	{
		base.Awake();
		SetWhatToDoOnClick(UnPause);
	}
	protected void OnGUI()
	{
		base.OnGUI ();
	}
	
	protected void UnPause()
	{
		Time.timeScale=1;
		
		Application.LoadLevel (Application.loadedLevel);
		
	}
}
