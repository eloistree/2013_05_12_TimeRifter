﻿using UnityEngine;
using System.Collections;

public class SBE_MainMenu : SBE_Button {

	
	protected void Awake()
	{
		base.Awake();
		SetWhatToDoOnClick(GoToMenu);
	}
	protected void OnGUI()
	{
		base.OnGUI ();
	}
	protected void GoToMenu()
	{
		Time.timeScale=1;

		fatherInterface.OnMenuRequested(SubWindowMenuRequest.MainMenu);

		IGameAbilities ingame = InGamePartnerInterface.ingame;
		if (ingame != null)
		ingame.GoTo (InGameToMenu.MainMenu);
		print ("Go to menu");
	}
}
