
using System;
using UnityEngine;

public abstract class SubWindowChildrenElemnt : ZoneElement, I_SubWindowChildren
{
	public GameObject fatherWindow;
	protected I_SubWindow fatherInterface;

	public int guiDepth =-110;


	protected override void Update()
	{
		base.Update();
		if(displayDebugZone)
			DisplayDebugZone();
		
	}

	protected void OnGUI(){
		
		GUI.depth = guiDepth;
	}

public  override void RefreshPositionAndSize()
{
		if(fatherWindow!=null){
			GUI_Element ge = fatherWindow.GetComponent (typeof(GUI_Element)) as GUI_Element;
			if(ge !=null){
				Rect f =ge.GetFinalPosition(); 
				finalLocalisation = MenuPositionTool.GetScreenLocalisationOf(positionType, mesure, ge.GetFinalPosition() , localisation , centered,GetSizeOnWidth(), GetSizeOnHeight());
			

			}
		}
}


	public override void Refresh (float time)
	{
		base.Refresh (time);
		finalLocalisation.width = 1000;
		finalLocalisation.height = 1000;

	}

	protected void Awake(){
		base.Awake ();

		Transform parent = this.transform.parent;
		//Does it has a parent
		if (parent == null) throw new UnityException ("A subwindow element has to be in SubWindow element."+this.gameObject);
		//If has a parent, does the user has define a SubWindow reference
		if ( (fatherWindow == null ||fatherInterface==null)  && parent!=null) {
			//No !!! He is the definition of lazy
			//Well then does the parent is a SubWindow reference
			fatherInterface = parent.GetComponent(typeof(I_SubWindow)) as I_SubWindow;
			// And the answer is ...
			if(fatherInterface!=null){
				//Yes it is :)
				fatherWindow=parent.gameObject;
			}

		}
		//If not sub found, ground the user
		if (fatherWindow == null) throw new UnityException ("A subwindow element has to reference at a SubWindow element or has one parent ref."+this.gameObject);
		if (fatherInterface == null) throw new UnityException ("Parent has to inherit of I_SubWindo interface"+this.gameObject);
		LinkedToTheFather ();

	


	}

	public void LinkedToTheFather ()
	{	fatherInterface.AddChildren (this,this.gameObject);
	}
		
}


