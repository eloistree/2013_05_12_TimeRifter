﻿using UnityEngine;
using System.Collections;

public  class SBE_Button : SubWindowChildrenElemnt {
	
	public GUIStyle backgroundStyle;
	public string backgroundText = "...";

	public delegate void  ToDoOnClick();
	private ToDoOnClick actionOnclick;

	private void ToDoDefault(){
		print ("Click ... Click");
	}
	public void Awake(){
		base.Awake ();
		SetWhatToDoOnClick (ToDoDefault );
	}

	protected void OnGUI()
	{
		base.OnGUI ();
		if(backgroundStyle.normal.background!=null && backgroundStyle.active.background!=null  ){
			if (GUI.Button (finalLocalisation, backgroundText, backgroundStyle)) {
				if(actionOnclick!=null)
					actionOnclick();	
			}
		}
		else if (GUI.Button (finalLocalisation, backgroundText)) {
			if(actionOnclick!=null)
				actionOnclick();	
		} 
	}


	public void SetWhatToDoOnClick(ToDoOnClick action) 
	{
		if (action != null) {
			actionOnclick=action;		
		}

	}

}
