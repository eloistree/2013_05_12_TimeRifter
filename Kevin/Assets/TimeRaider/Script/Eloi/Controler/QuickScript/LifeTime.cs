﻿using UnityEngine;
using System.Collections;

public class LifeTime : MonoBehaviour {

	public float lifeTime = 3f;

	public void Update()
	{

		lifeTime-= Time.deltaTime;
		if(lifeTime<=0) Destroy(this.gameObject);
	}
}
