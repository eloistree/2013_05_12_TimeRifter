using UnityEngine;
using System;

public class GUI_CrystalImpl : GUI_Crystal
{

	public GUIStyle textStyle;
	public Texture2D txtCrystal;
	public float betweenDistance=13f;
	public float width=14f,height=14f;


	void OnGUI()
	{
		if(txtCrystal!=null){
			int c = crystal;
			if (c<0) c=0;

			GUI.Label(finalLocalisation, String.Format("{0:000}", c), textStyle);
			Rect r = finalLocalisation;
			r.x+=betweenDistance;
			r.width= width;
			r.height=height;
			GUI.DrawTexture(r, txtCrystal);
		}
	}
}
		


