using System;
using UnityEngine;

public  abstract class GUI_Crystal : ZoneElement, I_GUI_Crystal
{
	public int crystal;
	public int maxCrystal;

	public void SetCrystals (int value)
	{
		if(value>=0)
		crystal = value;
	}

	public void SetMaxCrystals (int value)
	{
		
		if(value>=0)
			maxCrystal = value;
	}


}


