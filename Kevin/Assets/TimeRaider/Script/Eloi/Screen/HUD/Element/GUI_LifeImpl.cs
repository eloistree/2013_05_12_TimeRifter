using UnityEngine;
using System;

public class GUI_LifeImpl : GUI_Life
{
	public Texture2D txtLife;
	public float padding =5f;
	public float width= 40f;
	public float height= 40f;



	public void OnGUI()
	{
		if(txtLife!=null){
			float x=finalLocalisation.x,y=finalLocalisation.y;
			y+=padding;
			for(int i =0; i< life && i<maxLife; i++)
			{
				x+=padding;
				GUI.DrawTexture(new Rect (x,y,width, height),txtLife);
				x+=width;

			}
		}
	}

}


