
using System;
using UnityEngine;

public class InGameHud : MonoBehaviour, I_InGameHud
{


	public GameObject objBoost;
	private IBoostButton boost;
	
	public GameObject objLife;
	private I_GUI_Life life;
	
	public GameObject objCrystal;
	private I_GUI_Crystal crystal;
	
	public GameObject objMeter;
	private I_GUI_Meter meter;



	void Awake()
	{
		
		boost = objBoost.GetComponent(typeof(IBoostButton)) as IBoostButton;
		if(boost==null) throw new UnityException("Boost is not define "+this.gameObject);
		
		life = objLife.GetComponent(typeof(I_GUI_Life)) as I_GUI_Life;
		if(boost==null) throw new UnityException("Life is not define "+this.gameObject);
		
		crystal = objCrystal.GetComponent(typeof(I_GUI_Crystal)) as I_GUI_Crystal;
		if(boost==null) throw new UnityException("Crystal is not define "+this.gameObject);
		
		meter = objMeter.GetComponent(typeof(I_GUI_Meter)) as I_GUI_Meter;
		if(boost==null) throw new UnityException("Meter is not define "+this.gameObject);
	}


	public void SetBoostPct (float pct)
	{
		boost.SetPercentLoad(pct);
	}

	public void SetLife (int number)
	{
		life.SetLife(number);
	}

	public void SetMaxLife (int value)
	{
		life.SetMaxLife(value);
	}

	public void SetCrystals (int number)
	{
		crystal.SetCrystals(number);
	}

	
	public void SetMaxCrystals (int value)
	{
		crystal.SetMaxCrystals(value);
	}

	public void SetMeters (int value)
	{
		meter.SetMeters(value);
	}


	public void HighlightBoost (bool onOff)
	{
		boost.Highlight(onOff);
	}


	public I_GUI_Life GetLife ()
	{
		return life;
	}

	public I_GUI_Crystal GetCrystal ()
	{
		return crystal;		
}

	public I_GUI_Meter GetMeter ()
	{
		return meter;			
	}
	
	public IBoostButton GetBoost ()
	{
		return boost;
	}	

}


