﻿using UnityEngine;
using System.Collections;

public interface I_InGameHud  {

	void SetBoostPct (float pct);
	void SetLife (int number);
	void SetMaxLife (int value);
	void SetCrystals (int number);
	void SetMeters (int value);
	void SetMaxCrystals (int value);
	void HighlightBoost(bool onOff);


	
	I_GUI_Life GetLife();
	I_GUI_Crystal GetCrystal();
	I_GUI_Meter GetMeter();
	IBoostButton GetBoost();
}
