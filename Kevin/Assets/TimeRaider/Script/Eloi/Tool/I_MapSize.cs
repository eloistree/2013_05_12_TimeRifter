using UnityEngine;

public interface I_MapSize
{
	
	 float GetDistanceFromStartInMeter(Vector2  element);
	 bool IsOutOfTheMap(Vector2  element );
	
	 float GetHeight();
	 float GetHeightInMeter();
	 float GetWidth();
	 float GetWidthInMeter();
		

}


