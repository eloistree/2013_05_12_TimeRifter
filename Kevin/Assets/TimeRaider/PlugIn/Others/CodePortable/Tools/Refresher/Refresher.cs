﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Refresher : MonoBehaviour {



	private Refresher INSTANCE ;
	public Refresher GetInstance() {return INSTANCE;}

	void Awake()
	{
		if(INSTANCE==null) INSTANCE=this;
		else {
			Debug.LogError("Only one refresher is accepted by scene, this one is delete:"+this +" ("+this.gameObject+")");
			Destroy(this);
		}

		GameObject[] objs = GameObject.FindObjectsOfType<GameObject>() as GameObject [];


		Refreshable r = null;
		int i =0;
		foreach(GameObject go  in objs)
		{

			r = go.GetComponent(typeof(Refreshable)) as Refreshable;
			if (r!=null)
				Add(r, r.GetRefreshType());
		}


	}

	private float time;
	
	public float sometime =1.5f;
	private float lastSometime ;
	private LinkedList<Refreshable> listSometime = new LinkedList<Refreshable>();

	public readonly float eachsecond =1f;
	private float lastEachsecond ;
	private LinkedList<Refreshable> listEachSecond = new LinkedList<Refreshable>();
	
	public float often =0.3f;
	private float lastOften;
	private LinkedList<Refreshable> listOften = new LinkedList<Refreshable>();

	public float quick =0.1f;
	private float lastQuick;
	private LinkedList<Refreshable> listQuick = new LinkedList<Refreshable>();



	void FixedUpdate () {
		time = Time.timeSinceLevelLoad;
		float tmpPassed=0f;


		tmpPassed= time-lastEachsecond;
		if( tmpPassed>=eachsecond)
		{
			lastEachsecond=time;
			Warn(listEachSecond,time);
		}

		
		tmpPassed= time-lastSometime;
		if( tmpPassed>sometime)
		{
			lastSometime=time;
		//	print ("Tic, tac   "+ time);
			Warn(listSometime,time);
		}

		
		tmpPassed= time-lastOften;
		if( tmpPassed>often)
		{
			lastOften=time;
			Warn(listOften,time);
		}

		
		tmpPassed= time-lastQuick;
		if( tmpPassed>quick)
		{
			lastQuick=time;
			Warn(listQuick,time);
		}
	
	}


	public void Add(Refreshable listener, RefreshType type)
	{

		switch(type)
		{
		case RefreshType.Quick: listQuick.AddFirst(listener); break;
		case RefreshType.Often: listOften.AddFirst(listener); break;
		case RefreshType.EachSecond: listEachSecond.AddFirst(listener); break;
		case RefreshType.Sometime: listSometime.AddFirst(listener); break;
		}
	}



	void  Warn(LinkedList<Refreshable> toRefresh, float time)
	{
		foreach(Refreshable r  in toRefresh)
			r.Refresh(time);
	}

	public int GetLenght(RefreshType type)
	
	{
		
		switch(type)
		{
		case RefreshType.Quick: return listQuick.Count; 
		case RefreshType.Often: return  listOften.Count; 
		case RefreshType.EachSecond: return  listEachSecond.Count; 
		case RefreshType.Sometime: return  listSometime.Count; 
		}
		return 0;

	}


	public enum RefreshType{ Sometime, EachSecond, Often, Quick, NotNow}
	

}




public interface Refreshable 
{
	
	void Refresh( float time);
	Refresher.RefreshType GetRefreshType();
}


