﻿using UnityEngine;
using System;
using System.Collections;

[ExecuteInEditMode()]
public class GridMeasureImpl : GridMesure {

	
	public enum MeasureType{PX, Meter}
	public  MeasureType measureType = MeasureType.PX;
	/**U * ratio = m :  1 unit * 1.2 ratio = 1.20 m*/
	public float ratioByUnity = 1.0f;	/**U * ratio = m :  1 unit * 1.2 ratio = 1.20 m*/

	Vector3 topLeft = new Vector3 ();
	Vector3 topRight = new Vector3 ()	;
	Vector3 botLeft = new Vector3 ()	;
	Vector3 botRight = new Vector3 ()	;

	public float GetDistanceFromStartInMeasureType(Vector2  element)
	{
		float val = base.GetDistanceBetweenTwoPoint(left.position.x,element.x)*ratioByUnity;
		
		
		return val<0f?0f:val; 
	}


	public string ToString(float value)
	{
		return ""+ String.Format( "{0,-10:F}" ,(value*ratioByUnity)) +" "+ (measureType==MeasureType.Meter?"m":"px");

	}


	public void Update()
	{
				if (Application.isEditor) {
						GridMeasureImpl grid = this;

		
						if (grid.IsValide ()) {

								float w = grid.GetWidth ();
								float h = grid.GetHeight ();
								width= grid.ToString(w);
								height= grid.ToString(h);
								
								topLeft.x = grid.left.position.x;
								topLeft.y = grid.top.position.y;
			
								topRight.x = grid.right.position.x;
								topRight.y = grid.top.position.y;
			
								botLeft.x = grid.left.position.x;
								botLeft.y = grid.bottom.position.y;
			
								botRight.x = grid.right.position.x;
								botRight.y = grid.bottom.position.y;
			
			
								Debug.DrawLine (topLeft, botLeft);
								Debug.DrawLine (topLeft, topRight);
								Debug.DrawLine (botRight, topRight);
								Debug.DrawLine (botRight, botLeft);
			
						}
				}
		}
}
