﻿using UnityEngine;
using System.Collections;

public abstract class GridMesure : MonoBehaviour {

		public Transform left;
		public Transform right;
		public Transform top;
		public Transform bottom;
	
		[HideInInspector]
		public string width ;
		
		[HideInInspector]
		public string height ;

		void Start () {
			
			if (left == null || right == null || top == null || bottom == null ) {
				
				throw new UnityException("Coordinate(s) is(are) not define");
			}
			
			
		}
		
		
		
		public bool IsOutOfTheMap(Vector2  element )
		{
			if(element.y < bottom.position.y)return true;
			else if(element.y > top.position.y)return true;
			else if(element.x < left.position.x)return true;
			else if(element.y > right.position.x)return true;
			return false;
		}
		
		public float GetHeight()
		{
			return GetDistanceBetweenTwoPoint(bottom.position.y,top.position.y);
		}

		
		public float GetWidth()
		{
			return GetDistanceBetweenTwoPoint(left.position.x,right.position.x);
		}
		
		
		protected float GetDistanceBetweenTwoPoint(float smallOne,float bigOne)
		{
			float xl =smallOne;
			float xr =bigOne;
			if(xr<xl)
			{	
				//	Debug.LogWarning("Please Bitch, the other left...");
				float tmp = xr;
				xr = xl;
				xl=tmp;
			}
			//  o--o   |
			if(xl<0f && xr<=0f)
			{
				return -xl+xr;
			}
			//  o--|--o
			else 
				if(xl<=0f && xr>=0f)
			{
				return Mathf.Abs(xl)+xr;
			}
			
			//  |o----o
			else 
				if(xl>=0f && xr>=0f && xr>=xl)
			{
				return xr-xl;
			}
			Debug.Log("There is something wrong");
			
			return 0;
		}
		


		public bool IsValide()
		{
			if (top == null || bottom == null || left == null || right == null)
							return false;
		return true;
		}
	}


