﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public  class GUI_Element : MonoBehaviour {

	public bool positionRefresh =false;
	public MenuPositionType  positionType = MenuPositionType.Center;
	public MenuMesureType mesure  = MenuMesureType.PX;

	/** OnWidth, the user ask to affect the zone by the width of the screen.
	 OnHeight, the user ask to affect the zone by the height of the screen.	
		None, the user ask the zone to be deformed by the size of the screen.
		Both, ??? [TO DO].
	 */

	public enum MenuResizeType {OnWidth, OnHeight,Both,None}
	/**Do the element is affected by the width of the screen or the height*/
	public MenuResizeType  resizeOn = MenuResizeType.OnWidth;

	/**Do the player want to center the object instead of the left corner position*/
	public bool centered = true;
	/**Please do not modify, the localisation the player ask.*/
	public Rect localisation  = new Rect (0,0,50,50);
	/**The localisation the player ask, but translate by the params request*/
	protected Rect finalLocalisation ; 

	protected virtual void Awake ()
	{
		RefreshPositionAndSize();
	}

	protected virtual void Update()
	{
		if(positionRefresh)
			RefreshPositionAndSize();
	
	}
	protected virtual void OnGUI()
	{

	}

	public virtual void RefreshPositionAndSize()
	{


		finalLocalisation = MenuPositionTool.GetScreenLocalisationOf(positionType, mesure, localisation , centered,GetSizeOnWidth(), GetSizeOnHeight());

	}

	public bool GetSizeOnWidth()
	{
		return MenuResizeType.OnWidth.Equals (resizeOn) || MenuResizeType.Both.Equals (resizeOn);
	}
	public bool GetSizeOnHeight()
	{
		
		return MenuResizeType.OnHeight.Equals (resizeOn) || MenuResizeType.Both.Equals (resizeOn);
	}

	public Rect GetFinalPosition() { return finalLocalisation;}
}
