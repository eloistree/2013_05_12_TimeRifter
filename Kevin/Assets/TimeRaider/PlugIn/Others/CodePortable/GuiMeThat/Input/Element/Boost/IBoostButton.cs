using System;
public interface IBoostButton: I_ZoneElement
		{


	
	float GetPercentLoad();
	float GetLastTimeClicked();
	float GetLastTimeActivated();
	bool  HasBeenClickRecently ( float time);
	void Activate();
	void Reset();
	bool IsItReady();
	void  SetPercentLoad ( float value);
	void Highlight (bool onOff);
}


