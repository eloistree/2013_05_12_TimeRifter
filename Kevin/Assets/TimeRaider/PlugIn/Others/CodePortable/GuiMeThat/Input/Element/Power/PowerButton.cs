using System;
using UnityEngine;
public abstract class PowerButton :ZoneElement, IPowerButton 
{

	public float lastTimeClicked;
	public float lastTimeActivated;
	public bool isReadyAndNotUse;

	public void Activate ()
	{
		lastTimeActivated= Time.timeSinceLevelLoad;
		isReadyAndNotUse= false;
	}

	public float GetLastTimeClicked ()
	{
		return lastTimeClicked;
	}
	
	public float GetLastTimeActivated ()
	{
		return lastTimeActivated;
	}
	
	public bool HasBeenClickRecently (float time)
	{
		return (Time.timeSinceLevelLoad-lastTimeClicked)<time;
	}

	public bool IsItReady()
	{
	
		return isReadyAndNotUse;
	}


	protected void Clicked(){			
		lastTimeClicked= Time.timeSinceLevelLoad;
		isReadyAndNotUse=true;
	}
	public abstract void SetIcon (UnityEngine.Texture2D icon);
}


