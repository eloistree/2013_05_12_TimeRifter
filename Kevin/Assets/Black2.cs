﻿using UnityEngine;
using System.Collections;

public class Black2 : MonoBehaviour {

	public string message = "Block";


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector2 player = GameObject.FindWithTag("Player").transform.position;
		Vector2 Sol = this.transform.position;

		if (player.y < Sol.y) 
		{
			this.collider2D.isTrigger = true;
		} 
		else 
		{
			this.collider2D.isTrigger = false;
		}
	}


}
