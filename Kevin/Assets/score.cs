﻿using UnityEngine;
using System.Collections;

public class score : MonoBehaviour {

	int metres = 0;
	float timeCurrent = 0.0f;
	float delay = 1.0f;
	string distance;
	float previousX;

	// Use this for initialization
	void Start () {
		//timeCurrent = Time.fixedTime;
		previousX = transform.position.x;
	}
	
	// Update is called once per frame
	void OnGUI () {

		if(transform.position.x - previousX >=1)
		{
			metres++;
			//Debug.Log (metres);
			//timeCurrent = Time.fixedTime;
			previousX = transform.position.x;
		}

		distance = metres.ToString();	

		GUI.Label (new Rect(10, 10, 100, 100), distance+("M"));

	}
	
}
