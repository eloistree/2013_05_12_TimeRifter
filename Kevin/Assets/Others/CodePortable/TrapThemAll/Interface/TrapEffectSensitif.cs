﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */
using UnityEngine;

/**This is not the trap to define what will arrive when the effect will be apply.
By implementing a class that inherit of this interface, you take the responsability to define it.
 */
public interface TrapEffectSensitif
{
	/**
	 * trapEffect: define what trap effect the developer
	 * has to implement in aim to respect 
	 * what the trap try to do to the target .
	 * 
	 * trap: is the gameobject that represent the "totality" of the trap.
	 * (totality= because a trap can be composed of
	 * several GameObjects in a GameObject.
	 */
	void ApplyEffect( Trap.TrapEffect [] trapeffect , GameObject trapRoot);
	
}