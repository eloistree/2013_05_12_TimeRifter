﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */


public interface DetectionTypeInterface
{
	
	/**Do the trigger of the trap can hear the object*/
	bool IsObjectSoundable 	();
	
	/**Do the trigger of the trap can see the object*/
	bool IsObjectVisible 	();
	
	/**Do the trigger of the trap can weight the object*/
	bool IsObjectPesable 	();
}