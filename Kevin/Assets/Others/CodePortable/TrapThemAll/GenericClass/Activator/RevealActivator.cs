﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class RevealActivator : MonoBehaviour {

	
	public GameObject trapTriggerListener;

	public void Start(){
		if (trapTriggerListener == null && this.gameObject.transform.parent!=null) {
			Trap trap = this.gameObject.transform.parent.GetComponent<Trap>()as Trap;
			if(trap!=null){ trapTriggerListener = this.gameObject.transform.parent.gameObject;
			}
		}
	}

	public void OnTriggerEnter2D(Collider2D col)
	{
		if (trapTriggerListener != null) {
			DetectionTypeInterface infoCol = col .gameObject .GetComponent(typeof(DetectionTypeInterface)) as DetectionTypeInterface;
			
			
			if(infoCol!=null)
			{
				Trap trap = trapTriggerListener.GetComponent<Trap>() as Trap;
				if(trap!=null)
				{
					
					trap.RevealeTrap();
				}		
			}
		}
		
	}
}
