﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class TrapActivator : MonoBehaviour {
	
	public GameObject trapTriggerListener;

	public enum ListenerType
		{ Sound, View, Proximity , All}

	public ListenerType DetectionType;

	public void Start(){
		if (trapTriggerListener == null && this.gameObject.transform.parent!=null) {
			Trap trap = this.gameObject.transform.parent.GetComponent<Trap>()as Trap;
			if(trap!=null){ trapTriggerListener = this.gameObject.transform.parent.gameObject;
			}
		}
	}
	

	public void OnTriggerEnter2D(Collider2D col)
	{
		//Collision détecté 
		if (trapTriggerListener != null) {
				Trap trap = trapTriggerListener.GetComponent<Trap> () as Trap;
				if (trap != null) {
				DetectionTypeInterface infoCol = col .gameObject .GetComponent(typeof(DetectionTypeInterface)) as DetectionTypeInterface;
				

				if(infoCol!=null)
					{

						if(DetectionType.Equals(ListenerType.All)){
							//une collision est détecté et les conditions sont remplie pour etre détecté
							trap.ActivateTrap ();
						}
						else if(DetectionType.Equals(ListenerType.Proximity) && infoCol.IsObjectPesable()){
							//une collision est détecté et les conditions sont remplie pour etre détecté
							trap.ActivateTrap ();
						}
						else if(DetectionType.Equals(ListenerType.View) && infoCol.IsObjectVisible()){
							//une collision est détecté et les conditions sont remplie pour etre détecté
							trap.ActivateTrap ();
						}
						
						else if(DetectionType.Equals(ListenerType.Sound) && infoCol.IsObjectSoundable()){
							//une collision est détecté et les conditions sont remplie pour etre détecté
							trap.ActivateTrap ();

					
						}
					}
				}		
		}
	}
}
