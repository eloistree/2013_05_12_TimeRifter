﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class TrapCollisionListener : MonoBehaviour {

	public GameObject behaviourManager;
	public bool listenCollision = true;
	public bool listenTrigger = true;
	public string tagActivator = "Player";
	public void Start(){
		if (behaviourManager == null && this.gameObject.transform.parent!=null) {
			Trap trap = this.gameObject.transform.parent.GetComponent<Trap>()as Trap;
			if(trap!=null){
				behaviourManager = this.gameObject.transform.parent.gameObject;
			}
		}
	}


	public  Trap IsItATrap(){
		if (behaviourManager != null) {
			return behaviourManager.GetComponent<Trap> () as Trap;
		}
		return null;
	}


	public void OnCollisionEnter2D(Collision2D col)
	{
		if(IsEqualsToTagThatActivate(col.gameObject)){
		Trap trap = IsItATrap ();

			if(trap!=null)
			{
				trap.OnCollisionEnterDectected(col);
			}
		}
	
	}

	public void OnCollisionStay2D(Collision2D col)
	{
		
		if(IsEqualsToTagThatActivate(col.gameObject)){
		Trap trap = IsItATrap ();
		
		if(trap!=null)
		{
			trap.OnCollisionDectected(col);
		}
		}
		
	}

	public void OnCollisionExit2D(Collision2D col)
	{
		if(IsEqualsToTagThatActivate(col.gameObject)){
		Trap trap = IsItATrap ();

			if(trap!=null)
			{
				trap.OnCollisionExitDectected(col);
			}
		}
	}
	
	public void OnTriggerExit2D(Collider2D col)
	{
//		print ("Exit:"+ col.gameObject);
		if(IsEqualsToTagThatActivate(col.gameObject)){
			Trap trap = IsItATrap ();
			
			if(trap!=null)
			{
				trap.OnCollisionExitDectected(col);
			}
		}
		
	}

	public void OnTriggerEnter2D(Collider2D col)
	{
		//This commentary is not deleted because a bug without solution can reapeared
		//Overload of the trigger by unity when using mecanim. If the trigger is overloaded the exit trigger will never work.
		//print ("Start:"+ this.gameObject.name +"->"+ this.gameObject.tag +"-->"+this.gameObject.transform.position );
		//print ("Start:"+ col.gameObject.name +"->"+ col.gameObject.tag +"-->"+col.gameObject.transform.position );
		if(IsEqualsToTagThatActivate(col.gameObject)){
			Trap trap = IsItATrap ();

			if(trap!=null)
			{
				trap.OnCollisionEnterDectected(col);

			}
		}

	}
	
	public void OnTriggerStay2D(Collider2D col)
	{
		
		//This commentary is not deleted because a bug without solution can reapeared
		//Overload of the trigger by unity when using mecanim. If the trigger is overloaded the exit trigger will never work.
		//print ("Colliding:"+ col.gameObject);
		if(IsEqualsToTagThatActivate(col.gameObject)){
			Trap trap = IsItATrap ();
			
			if(trap!=null)
			{
				trap.OnCollisionDectected(col);
			}
		}
		
	}

	
	private bool IsEqualsToTagThatActivate(GameObject o){
		if(o==null || tagActivator==null) return false;
		return tagActivator.Equals(o.tag);
	}
}
