﻿using UnityEngine;
using System.Collections;

public class ImpultionScript : MonoBehaviour {
	
	public float force =300;
	public float torque =300;
	public Vector2 direction = -Vector2.right;
	void Start () {
	
		if (gameObject.rigidbody2D != null) {
			if(force!=0)
			gameObject.rigidbody2D.AddForce(direction*force);		
			if(torque!=0)
			gameObject.rigidbody2D.AddTorque(torque);

		}

	}

}
