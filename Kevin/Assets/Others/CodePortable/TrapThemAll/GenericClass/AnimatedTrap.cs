﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class AnimatedTrap : TriggeredTrap {
	
	/**Engine represent the animator of the object threat by unity.
	 * The most zone trap need a simple script
	 * because the complexity is on the move of the object.*/
	public Animator engine ;
	
	/**Give the possibility to apply the effect of the trap only on a short time between start and stop apply*/
	public float WhenToStartApply=0f;
	
	/**Give the possibility to apply the effect of the trap only on a short time between start and stop apply
	 * -1 = never stop apply after the trap start apply*/
	public float WhenToStopApply=-1;
	
	/**The animator has to implement a params named x to be activated*/
	public  string AnimatorParamsStart ="Actif";
	
	/**The animator has to implement a params named x to be revealed*/
	public  string AnimatorParamsReveal ="Reveal";
	
	public new void Start(){
		base.Start ();
		
		
	}
	protected override void ApplyEffectTo(GameObject objToApply)
	{

		float time = Time.timeSinceLevelLoad;
		float timeSinceActif = time - ActivatedTrapTime;
		if(WhenToStopApply<0 && timeSinceActif>=WhenToStartApply) 
				base.ApplyEffectTo(objToApply);
		else if (WhenToStopApply>WhenToStartApply && timeSinceActif>=WhenToStartApply && timeSinceActif<=WhenToStopApply)
				base.ApplyEffectTo(objToApply);


	}
	
	
	protected override void OnTrapActivate()
	{
		base.OnTrapActivate ();
		block.SetActive (true);
		if (engine !=null) engine.SetBool(AnimatorParamsStart, true);
		
		
	}
	protected override void OnTrapReveal()
	{
		base.OnTrapReveal ();
		if (engine !=null) engine.SetBool(AnimatorParamsReveal, true);
		
		
	}

}
