﻿using UnityEngine;
using System.Collections;

public class DEL_PlayerBehviour : MonoBehaviour , DetectionTypeInterface, TrapEffectSensitif{

	public bool Visible = true;
	public bool Soundable = true;
	public bool Pesable = true;
	
	public float actualspeed = 5f;
	public float normalSpeed = 8f;
	public float recoverSpeed = 5f;

	public int life = 15;

	void Start () {
	
	}
	public float refreshSpeedTime;
	void Update () {
		if(actualspeed<normalSpeed)
		actualspeed+= recoverSpeed*Time.deltaTime;

		float time = Time.timeSinceLevelLoad;
		if(time-refreshSpeedTime>0.2f){
			
//			DEL_MoveMan pc = this.gameObject.GetComponent<DEL_MoveMan>() as DEL_MoveMan;
			refreshSpeedTime=time;
			
		//	pc.speed=actualspeed;
		}

	}

	public void Dead()
	{

		Destroy (this.gameObject);
	}

	public bool IsObjectSoundable ()
	{
		return Soundable;
	}

	public bool IsObjectVisible ()
	{
		return Visible;
	}

	public bool IsObjectPesable ()
	{
		return Pesable;
	}

	public float lastTimeEffectApply;


	public void ApplyEffect (Trap.TrapEffect [] trapeffect, GameObject trap)
	{
//		Debug.Log ("Trap applied effect : " + trap + "    " + trapeffect);
//		float time = Time.timeSinceLevelLoad;

//			DEL_MoveMan pc = this.gameObject.GetComponent<DEL_MoveMan>() as DEL_MoveMan;

		foreach(Trap.TrapEffect effect in trapeffect){
			switch(effect)
			{
				
				case Trap.TrapEffect.Kill:  
				PlayerIsDeath(); break;
			case Trap.TrapEffect.Dommage: life--; break; 
			case Trap.TrapEffect.Slow: actualspeed-=3f; break; 
			case Trap.TrapEffect.Fear: actualspeed= -actualspeed; break;  
			case Trap.TrapEffect.PushOver :
				
				if(this.gameObject.rigidbody2D!=null)
				{
					this.gameObject.rigidbody2D.AddForce(new Vector2(-1000,1000));
				}
				break; 
			case Trap.TrapEffect.Stun: actualspeed=0f; break; 

			}
				
		}

			
		//	pc.speed=actualspeed*100f;
	
	}

	private void PlayerIsDeath()
	{

		//yield return new WaitForSeconds(0.2f);
		 DEL_QuickReload.RestartScene();
	}

}
