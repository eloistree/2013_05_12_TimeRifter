﻿using UnityEngine;
using System.Collections;

public class DEL_MoveMan : MonoBehaviour {
	
	public float forceToward=20f;
	public float forceJump=1000f;
	public float delay= 0.25f;
	public float lastTimeRefresh= 0.25f;
	// Update is called once per frame
	public bool grounded;
	public bool blockedWay;
	public bool jump;
	public Transform groundCheck;
	public Transform wayCheck;
	public float maxSpeed=30f;
	void Update()
	{
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1<<LayerMask.NameToLayer("Ground"));  
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		blockedWay = Physics2D.Linecast(transform.position, wayCheck.position, 1<<LayerMask.NameToLayer("Ground"));  


		// If the jump button is pressed and the player is grounded then the player should jump.
		if(Input.GetButtonDown("Jump") && grounded)
			rigidbody2D.AddForce(Vector2.up  * forceJump);
		if(Input.GetKeyDown(KeyCode.DownArrow) && !grounded)
			rigidbody2D.AddForce(-Vector2.up  * forceJump);
		if(Input.GetKeyDown(KeyCode.LeftArrow) )
			rigidbody2D.AddForce( new Vector2(-1f,0.5f)  * forceJump*2f);
		if (Input.GetKeyDown (KeyCode.RightArrow) ) 
		{
			//Vector2 v = transform.position;
			//v.x+=10f;
			transform.Translate(Vector2.right*10f);

		}

	}


	void FixedUpdate () {


		//this.gameObject.transform.Translate(Vector2.right*forceToward*Time.fixedDeltaTime);
		// Cache the horizontal input.

		float h = 1;//Input.GetAxis("Horizontal");
		if (blockedWay) {
			h=0;
			rigidbody2D.velocity=new Vector2(rigidbody2D.velocity.x*0.55f, rigidbody2D.velocity.y);
		}
					
		
		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxforceToward yet...
		if(h * rigidbody2D.velocity.x < maxSpeed)
			// ... add a force to the player.
			rigidbody2D.AddForce(Vector2.right * h * forceToward);
		
		// If the player's horizontal velocity is greater than the maxforceToward...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxforceToward in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);


	}
}