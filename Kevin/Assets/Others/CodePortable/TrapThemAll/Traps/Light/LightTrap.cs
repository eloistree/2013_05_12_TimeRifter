﻿using UnityEngine;
using System.Collections;

public class LightTrap : TriggeredTrap {

	public enum LightType{ BlackOut, Blinking}
	public LightType lightType= LightType.BlackOut;
	public GameObject blackMask ;	
	private Material blackMaskMaterial ;
	public float pct =0.8f;


	public new void Start(){
		base.Start ();
		if (blackMask  == null) {
			if(!NoWarning)
			Debug.LogWarning ("No black mask associate to mask the camera view");
			throw new UnityException("Not light assigned");
		}
		if (blackMask.renderer  == null || blackMask.renderer.material  == null  ) {
			if(!NoWarning)
			Debug.LogWarning ("Matrial is required on a black mask for this class");
			throw new UnityException("Matrial is required on a black mask for this class");
		}

		if (blackMask.renderer.material.shader  == null || ! blackMask.renderer.material.shader.name.Equals("Transparent/Diffuse") ) {
			if(!NoWarning)
			Debug.LogWarning ("Transparent Shader is required on a black mask for this class");
			throw new UnityException("Transparent Shader is required on a black mask for this class");
		}
		blackMaskMaterial = blackMask.renderer.material;
		DisplayNight (false);
	}
	
	
	
	protected   override void OnTrapActivate()
	{
		base.OnTrapActivate ();
		
	}
	

	public override void OnCollisionEnterDectected(Collider2D col)
	{	

		if (col.gameObject.tag.Equals ("Player") && LightType.BlackOut.Equals(lightType)) 
		{
			float pct  =this.pct*255f;
			blackMaskMaterial.color = new Color(0,0,0,pct);
			
			DisplayNight(true);
		}
	}
	public override void OnCollisionExitDectected(Collider2D col)
	{
			
		if (col.gameObject.tag.Equals ("Player") && LightType.BlackOut.Equals(lightType)) 
		{
			DisplayNight(false);
		}
	}

	public void DisplayNight (bool onOff)
	{
		if (onOff) {
			blackMask.SetActive (true);
			DisplayNight(pct);
		}
		else {
			blackMask.SetActive (false);
			DisplayNight(0f);
		}
	}

	private void DisplayNight(float pct)
	{

		if (pct > 1f)
			pct = 1f;
		else if (pct < 0f)
			pct = 0f;
		blackMaskMaterial.color = new Color(0,0,0,pct);

	}

}
