﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class BlinkedTrap : TriggeredTrap {

	public GameObject RefToLineObj;
	public float TimeOnOffSequence= 1f;
	public float RandomRangSequence=0.5f;
	public bool onOff=true;
	 float lastSwitchTime;
	 float newDelay=1f;


	public new void Start(){
		base.Start ();
		if (block == null) {
			if(!NoWarning)
			Debug.LogWarning ("The reference to the clipping laser is not define");
			throw new UnityException ("The reference to the clipping laser is not define");
		}
	}


	public void Update(){
		if (RefToLineObj!=null&&ActivatedTrap) {
			float time = Time.timeSinceLevelLoad - lastSwitchTime;
			if(time>newDelay)
			{
				onOff=!onOff;
				RefToLineObj.SetActive(onOff);

				newDelay= TimeOnOffSequence+ Random.Range(0f,RandomRangSequence);
				lastSwitchTime= Time.timeSinceLevelLoad;
			}

				
		}

	}
	protected  override void OnTrapActivate(){
		base.OnTrapActivate ();
	}

	public override void OnCollisionEnterDectected(Collider2D col)
	{
		if (col.gameObject.tag.Equals ("Player")) 
		{
			ApplyEffectTo(col.gameObject);
			
		}
	}
	public override void OnCollisionExitDectected(Collider2D col)
	{
		
	}public override void OnCollisionDectected(Collider2D col)
	{
		
	}
}

