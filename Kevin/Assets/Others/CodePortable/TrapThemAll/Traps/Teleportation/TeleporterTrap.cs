﻿using UnityEngine;
using System.Collections;

public class TeleporterTrap : TriggeredTrap {
	
	public Transform teleportationPosition;
	public float delayTeleportation = 0;
	
	public new void Start()
	{
		base.Start();
		if (teleportationPosition.position == Vector3.zero)
			if(!NoWarning)
			Debug.LogWarning ("The teleport location point is not define");
	
	}
	public override void OnCollisionEnterDectected (Collision2D col)
	{
		
		CollisionEffect (col.gameObject);
	}

 	public override void OnCollisionEnterDectected (Collider2D col)
	{
		CollisionEffect (col.gameObject);
	}
	private void CollisionEffect( GameObject o){
		if (o.tag.Equals ("Player")) 
		{
			//ApplyEffectTo(trapEffect,root, o);
			Teleport(o);
		}
	}


	private void Teleport(GameObject obj)
	{
		obj.transform.position = teleportationPosition . position;
	}


}
