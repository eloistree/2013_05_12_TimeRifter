﻿using UnityEngine;
using System.Collections;

public class FunctionPlayer : MonoBehaviour 
{
	public string Message;

	public float BarLife;
	public float Life;
	public float LifeMax;
	
	public int Niv;

	public Texture2D BarEmpty;
	public Texture2D BarFull;

	public GUIStyle NivStyle;

	// Use this for initialization
	void Start () 
	{
		Message = "";
		LifeMax = 100;
		Life = LifeMax;
	}
	
	// Update is called once per frame
	void Update () 
	{
		PlayerMove();
		VerifLife();
	}

	public void PlayerMove()
	{
		transform.Translate (4 * Time.deltaTime,0, 0);

		if(Input.GetKeyDown(KeyCode.Space)) 
		{
			//transform.Translate(0,100 * Time.deltaTime,0);
			this.rigidbody2D.AddForce(Vector2.up * 400.0f);
		}
	}
	
	public void AddNiv()
	{
			Niv++;
	}

	public void Block()
	{
		if(this.collider2D.isTrigger == true)
		{
			this.collider2D.isTrigger = false;
		}
		else
		{
			this.collider2D.isTrigger = true;
		}

	}


	public void VerifLife()
	{
		BarLife = (Life / LifeMax) * 400;

		if(Life >= LifeMax)
		{
			Life = LifeMax;
		}
		else if(Life <= 0)
		{
			Life = 0;
			Dead();
		}
		else
		{
			Message = "";
		}
	}

	public void Damage()
	{

	}

	public void Dead()
	{
		Message = "Vous ètes mort !!!";
	}


	public void OnGUI()
	{
		// GUI Niv
		GUI.Label(new Rect(5, 5, 100, 100), Niv.ToString(), NivStyle);

		// GUI Dead
		GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, Message.Length, 100), Message);

		// GUI Vie
		GUI.DrawTexture(new Rect(Screen.width / 2, 10, 400, 10), BarEmpty);
		GUI.DrawTexture(new Rect(Screen.width / 2, 10, BarLife, 10), BarFull);
	}





}
