﻿using UnityEngine;
using System.Collections;

public class Diamond : MonoBehaviour {

	public GameObject gameData;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
		{
		Debug.Log ("c'est collisione");
			if (col.gameObject.tag == "Player") 
				{
					if(gameObject.name == "diamond")
					{
						gameData.SendMessage ("moreDiamond", 1);
						PlayerPrefs.SetInt ("diamandOwnByPlayer", 1);
					}
					else
					{
						gameData.SendMessage ("moreDiamond", 10);
						PlayerPrefs.SetInt ("diamandOwnByPlayer", 10);
					}
					Destroy (gameObject);
				}
		}
}
