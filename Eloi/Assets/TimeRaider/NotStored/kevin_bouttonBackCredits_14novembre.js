﻿#pragma strict

public var background : Texture2D;
public var backgroundClicked : Texture2D;
public var imageCentrale : GameObject;
public var fondDidacticiel : GameObject;

function Start () {
guiTexture.texture = background;
guiTexture.pixelInset = new Rect(10.14,fondDidacticiel.guiTexture.texture.height/2 - background.height/2,background.width, background.height);
}

function Update () {

}

function OnMouseUp()
	{
		guiTexture.texture = background;
		imageCentrale.SendMessage("backForward", true);
	}

function OnMouseDown()
	{
		guiTexture.texture = backgroundClicked;
	}