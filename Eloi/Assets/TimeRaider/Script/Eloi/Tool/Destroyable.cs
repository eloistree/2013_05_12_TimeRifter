using UnityEngine;


/**Class that provide the possbility to ask to destroy the object
 * in a other wait that only Destroy(blabla)*/
public abstract  class Destroyable : MonoBehaviour
{
	/**Ask to destroy the object in a other wait that only Destroy(blabla)*/
	public void Destroy(float time, float rubOutTime)
	{
		RubOut(time,rubOutTime);
		BeforeRemove(time,rubOutTime);
		Destroy(this.gameObject);
	}
	/**Is there a code to apply to code to "rub out" the object, transparence, AddTorque .. ? */
	protected abstract void RubOut(float time,float timeBeforeRemove);
	/**Is there a code you want the object to do before be delete of the scene*/
	protected abstract void BeforeRemove(float time,float timeBeforeRemove);
}
