using System;
using UnityEngine;

public abstract class GUI_Life : ZoneElement, I_GUI_Life
{
	public int life;
	public int maxLife;


	public void SetLife (int value)
	{
		if(value>=0)
			life = value;
	}

	public void SetMaxLife (int value)
	{
		if(value>=0)
			maxLife = value;
	}
}


