﻿using UnityEngine;

public interface I_Player  {

	/**Add listener at the player transmitter if it inherite of I_PlayerListener*/
	void AddListener(I_PlayerListener listener);
	/**Remove all listener of the player*/
	void ClearListener();

	
	/**Retunr the position of the player in the scene*/
	Vector2 GetPosition();
	
	/**Is the player is over the ground*/
	bool IsOverGround();
	
	/**Can the player have enought ground to be able to jump*/
	bool IsOverGroundToJump();
	/**Do the player has a wall that face up him*/
	bool IsWallBlock();

	void SetSpeedInfo(float speed);

	void  SetState(PlayerState value);
	PlayerState GetState();

	void SetMindState(PlayerMindState value);
	PlayerMindState GetMindState();


}

/**Represent the state of the player
 * Waiting: the player is waiting form to long the character is bored
 * NotMoving: waiting order to run
 * Running: is running
 * Jumping: the character jump in a direction
 * Sliding: is sliding = quick move toward with a collision reduce in height
 * JumpingBack: the player do a salto behind him
 * Teleporting: the player is teleporting = effect and blinked
 * Teleporting: the player is deaht :)
 * */
public enum PlayerState{Waiting, NotMoving, Running, Jumping,Sliding,JumpingBack, Teleporting, Death}

/** Represent the ability to display the state of mind of the player
 * None: nothing is display
 * Stun: display star up on the head
 * Speak: display a speaking "bla bla bla" bubble up on the head of the player
 * Think: display a gears bubble 
 * Fear: display a demon icon to represent that the player is fear and lose the control of the movement
 */
public enum PlayerMindState{None,Stun,Speak,Think, Fear}




