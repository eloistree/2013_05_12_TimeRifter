﻿using UnityEngine;
using System.Collections;

public abstract class  Action : MonoBehaviour, I_Action {

	
	public Vector2 jump=new Vector2(0.5f,1f);
	public float jumpRatio=1f;

	public Vector2 jumpBack=new Vector2(0.5f,1f);
	public float jumpBackRatio=1f;

	public Vector2 slide=new Vector2(1f,0.17f);
	public float slideRatio=1f;

	public Vector2 dashDown=new Vector2(1f,-0.6f);
	public float dashDownRatio=1f;

	public Vector2 teleport=new Vector2(20f,0f);
	public float teleportRatio=1f;
	public float teleportTimeActivation=0.5f;
	public Transform entityLocalisation;
	





	public   void  DoThisAction (Actions action)
	{

		switch(action)
		{
		case Actions.Dash: 		DoThisAction (dashDown		, action); 		break;
			case Actions.Jump: 		DoThisAction (jump		, action); 		break;
			case Actions.JumpBack: 	DoThisAction (jumpBack	, action);	 	break;
			case Actions.Teleport:	DoThisAction (teleport	, action); 		break;
			case Actions.Slide: 	DoThisAction (slide		, action); 		break;

		}
	}

	public  void DoThisAction (Vector2 direction, Actions action)
	{


		Rigidbody2D rb = entityLocalisation.rigidbody2D;

		ActionStartApply( direction,  entityLocalisation,  action);
		switch(action)
		{
		case Actions.Dash: 		PushWithTheForceLuke (	direction * dashDownRatio				, rb);break;
			case Actions.Jump: 		PushWithTheForceLuke (	direction * jumpRatio				, rb);break;
			case Actions.JumpBack: 	PushWithTheForceLuke (	direction * jumpBackRatio		, rb);break;
			case Actions.Teleport:	Teleport (				direction	* teleportRatio		, entityLocalisation);break;
			case Actions.Slide: 	PushWithTheForceLuke (	direction * slideRatio				, rb);break;
				
		}
		ActionEndApply( direction,  entityLocalisation,  action);
	}

	public  void SetAction (Vector2 direction, float multiplicatorRatio,  Actions action)
	{	
		
		switch(action)
		{
		case Actions.Dash: 		dashDown= 		direction ; 	dashDownRatio = multiplicatorRatio;		break;
		case Actions.Jump: 		jump= 		direction ; 	jumpRatio = multiplicatorRatio;		break;
		case Actions.JumpBack: 	jumpBack= 	direction ; 	jumpBackRatio = multiplicatorRatio;	break;
		case Actions.Teleport:	teleport= 	direction ; 	teleportRatio = multiplicatorRatio;	break;
		case Actions.Slide: 	slide= 		direction ; 	slideRatio = multiplicatorRatio;	break;

			
		}
		
	}




	private void PushWithTheForceLuke(Vector2 direction, Rigidbody2D obj)
	{
		if(obj!=null)
		{
			obj.AddForce(direction);
		}
	}


	private void Teleport(Vector2 direction, Transform obj)
	{
		if(obj!=null){
			Vector2 pos 	 = 	obj.transform.position;
			pos 	+=	direction;
			obj.transform.position = pos;
		}

	} 

	
	protected abstract void ActionStartApply(Vector2 direction, Transform obj, Actions action);
	protected abstract void ActionEndApply(Vector2 direction, Transform obj, Actions action);

}
