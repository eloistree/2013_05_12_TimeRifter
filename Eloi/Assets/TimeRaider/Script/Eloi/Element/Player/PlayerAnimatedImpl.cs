using System;
using UnityEngine;

public class PlayerAnimatedImpl : PlayerImpl
{
						
			public Animator bodyEngine;
			public string ParamsJump = "Jump";
			public string ParamsBackJump = "JumpBack";
			public string ParamsSlide = "Slide";
			public string ParamsStop = "Stop";
			public string ParamsRunning = "Running";
			public string ParamsDeath = "Death";



	public override void SetState (PlayerState value)
	{
		base.SetState (value);

		if (bodyEngine != null) {
						switch (value) {
						case PlayerState.Death:
								break;		
						case PlayerState.Jumping:
								bodyEngine.SetTrigger (ParamsJump);
								break;	
						case PlayerState.JumpingBack:
								bodyEngine.SetTrigger (ParamsBackJump);
								break;	
						case PlayerState.Sliding:
								bodyEngine.SetTrigger (ParamsSlide);
								break;	
						case PlayerState.NotMoving:
						case PlayerState.Waiting:
								bodyEngine.SetBool (ParamsStop, true);
								bodyEngine.SetBool (ParamsRunning, false);
								break;	

						case PlayerState.Running: 
								bodyEngine.SetBool (ParamsRunning, true);
								bodyEngine.SetBool (ParamsStop, false);
								break;	
						case PlayerState.Teleporting:
								break;	

		
						}
				}
	}
}


