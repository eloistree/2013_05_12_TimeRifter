﻿using UnityEngine;
using System.Collections;

/**This class take in charge a object that is going to push by force toward the map.
 * This force will depend of the speed that is setting.
 * There is 3 layer, Stun , Slow , InputControl
 * Stun stop the player speed to zero for until it is reactivated
 * Slow affect the speed in the min max limit possible
 * InputControl, affect the speed but in min max limit with a range less effective
 */
public interface I_Slider  {

	/**Define what speed the entity is going to run all the time if not affected.
	 * If the speed is not more slowed, stun or other affectation, the speed 
	 * is going to recover the normal speed
	 */
	void SetNormalSpeed(float value); 

	/**
	 * Define the limit of speed reachable (exception of the stun)
	 * min: the entity never run under this limit
	 * max: the entity never run over this limit
	 */
	void SetMinAndMaxSpeed(float min, float max);
	
	/**
	 * Define the limit of speed reachable by a affectation 
	 * of the controlers
	 * min: the entity never run under this limit by using the controler
	 * max: the entity never run over this limit by using the controler
	 */
	void SetMinAndMaxSpeedAffectable(float min, float max);

	/** Decrease or Incress the speed of the entity*/
	void AffectSpeedBy(float value, SliderAffectation affectationType); 
	
	/** Set the speed in the limit of the slider affectation*/
	void SetSpeed(float value, SliderAffectation affectationType);
	
	/** Get the speed */
	float GetSpeed();
	

	/**The speed fixed to zero  for x seconds*/
	void StopRunning(bool stopToRun, float seconds); 


	void SetRecoveringValue(float acceleration, float deceleration);

	
	float GetMinSpeed();
	float GetMaxSpeed();
	
	float GetMinSpeedSlow();
	float GetMaxSpeedSlow();
	
	float GetNormalSpeed();


}

public enum SliderAffectation {FullControl, Stun, Slow, Request }
