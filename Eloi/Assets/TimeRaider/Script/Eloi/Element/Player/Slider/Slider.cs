﻿using UnityEngine;
using System.Collections;

public abstract class Slider : MonoBehaviour , I_Slider {

	
	public float speed;
	
	public float normalSpeed =25f;


	public float minSpeed=3f,maxSpeed=50f;
	public float minAffectableSpeed=12f, maxAffectableSpeed=32f;

	public float accelerationRecovery=20f, decelerationRecovery=5f;

	public float timeStun;


	
	public float maxJump=20f;




	protected void FixedUpdate()
	{

		float timePassed = Time.fixedDeltaTime;
		StunManipulation(timePassed);
		SpeedManipulation(timePassed);
	}


	private void SpeedManipulation(float timePassed)
	{
		if(timeStun<=0f){
			float newSpeed = speed;
			
			if(speed>normalSpeed) newSpeed -=decelerationRecovery*timePassed;
			if(speed<normalSpeed) newSpeed +=accelerationRecovery*timePassed;
			speed = newSpeed;
	
		}
	}
	private void StunManipulation(float timePassed)
	{

		timeStun-=timePassed;
		if(timeStun<0f)timeStun=0f;
	}

	public void SetNormalSpeed (float value)
	{
		if(value >0)normalSpeed = value;
	}

	public void SetMinAndMaxSpeed (float min, float max)
	{
		
		if(min>max)
		{
			float tmp = max;
			max=min;
			min=tmp;
		}
		if(min >0)minSpeed = min;
		if(max >0)maxSpeed = max;

	}

	public void SetMinAndMaxSpeedAffectable (float min, float max)
	{
		if(min>max)
		{
			float tmp = max;
			max=min;
			min=tmp;
		}
		if(min >0)minAffectableSpeed = min;
		if(max >0)maxAffectableSpeed = max;
	

	}


	public void SetRecoveringValue (float acceleration, float deceleration)
	{	
		
		accelerationRecovery = Mathf.Abs(acceleration);
		decelerationRecovery = Mathf.Abs(deceleration);
	}

	public void AffectSpeedBy (float value, SliderAffectation affectationType)
	{
		float newSpeed = speed+value;
		newSpeed =  RestrictValue(newSpeed, affectationType);
		speed = newSpeed;
	}

	public void SetSpeed (float value, SliderAffectation affectationType)
	{
		
		speed  =  RestrictValue(value, affectationType);

	}

	public void StopRunning (bool stopToRun, float seconds)
	{
		if(stopToRun){
			speed= RestrictValue(0f, SliderAffectation.Stun);

			if(seconds > timeStun) timeStun=seconds;
		}
		else timeStun=0f;
	}



	/**Manadge the ability to modify the speed of the and return the value
	 corresponding at the layer access*/
	private float RestrictValue(float value, SliderAffectation affectType)
	{
		 if (SliderAffectation.Request.Equals(affectType))
		{
			if(value<minAffectableSpeed) value = minAffectableSpeed;
			else if(value>maxAffectableSpeed) value = maxAffectableSpeed;

			return value;
		}
		
		else if (SliderAffectation.Slow.Equals(affectType))
		{
			if(value<minSpeed) value = minSpeed;
			else if(value>maxSpeed) value = maxSpeed;

			return value;
		}

		return value;
	}


	public float GetMinSpeed ()
	{
		return minAffectableSpeed;
	}

	public float GetMaxSpeed ()
	{
		
		return maxAffectableSpeed;
	}

	public float GetMinSpeedSlow ()
	{
		return minSpeed;
	}

	public float GetMaxSpeedSlow ()
	{
		return maxSpeed;
	}

	public float GetNormalSpeed ()
	{
		return normalSpeed;
	}

	public float GetSpeed ()
	{
		return speed;
	}

}
