﻿using UnityEngine;
using System.Collections;

public interface I_PlayerListener  {

	void PlayerDommaged(int value);
	void PlayerDeath();
	void PlayerIsAffectedBy(PlayerBonusMalus value);

}
/***
 * Define the possibility of effect that are applicable to the player
 * 
 * */
public enum PlayerBonusMalus { Death, Slow, Fear, PlagueyTalk, Dommage, Stun ,Pushover, QuickSand, SpeedUp }
