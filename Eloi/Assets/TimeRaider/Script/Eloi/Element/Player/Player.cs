﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
public abstract class Player : MonoBehaviour,I_Player, DetectionTypeInterface, TrapEffectSensitif{
	


	private LinkedList<I_PlayerListener> listenerList = new LinkedList<I_PlayerListener>();
	public void AddListener (I_PlayerListener listener)
	{
		if (listener!=null) {listenerList.AddFirst(listener);}

	}

	public void ClearListener ()
	{
		listenerList.Clear();
	}

	public PlayerState state= PlayerState.Running;
	public PlayerMindState mindState= PlayerMindState.None;

	public bool Visible = true;
	public bool Soundable = true;
	public bool Pesable = true;

	protected float speedInfo;

		public bool IsObjectSoundable ()
		{
			return Soundable;
		}
		
		public bool IsObjectVisible ()
		{
			return Visible;
		}
		
		public bool IsObjectPesable ()
		{
			return Pesable;
		}
		
	
		
		
		
		public virtual void ApplyEffect (Trap.TrapEffect [] trapeffect, GameObject trap)
		{
			foreach(Trap.TrapEffect effect in trapeffect){
				foreach(I_PlayerListener listener in listenerList)
				{	
				if(listener!=null){
					// the switch  make the transition between two different world: Trap and Ingame
					switch(effect)
						{
						

						case Trap.TrapEffect.Kill: 		listener.PlayerIsAffectedBy(PlayerBonusMalus.Death);break;
						case Trap.TrapEffect.Dommage:	listener.PlayerIsAffectedBy(PlayerBonusMalus.Dommage);break; 
						case Trap.TrapEffect.Slow: 			listener.PlayerIsAffectedBy(PlayerBonusMalus.Slow);break; 
						case Trap.TrapEffect.Fear: 			listener.PlayerIsAffectedBy(PlayerBonusMalus.Fear);break;  
						case Trap.TrapEffect.PushOver :			listener.PlayerIsAffectedBy(PlayerBonusMalus.Pushover);break; 
						case Trap.TrapEffect.Stun:  		listener.PlayerIsAffectedBy(PlayerBonusMalus.Stun);break; 
						case Trap.TrapEffect.PlagueyTalk:  		listener.PlayerIsAffectedBy(PlayerBonusMalus.PlagueyTalk);break; 
						
						}
						
					}
				}
			}

			
			
		}

	
	public abstract Vector2 GetPosition ();


	public  abstract bool IsOverGround ();
	public  abstract bool IsOverGroundToJump ();
	public abstract  bool IsWallBlock ();

	/**Set the player state. 
	* The implementation of Player has to define what to do in unity to the player view when the state change
	 */
	public virtual void SetState (PlayerState value)
	{

		if(state!=value){
			state=value;}
	}

	public PlayerState GetState ()
	{return state;
	}
	/**Set  the player's mind state. 
	* The implementation of Player has to define what to do in unity to the player view when the mind state change
	 */

	public virtual void SetMindState (PlayerMindState value)
	{
		if(mindState!=value){
			mindState=value;}
	}

	public PlayerMindState GetMindState ()
	{return mindState;
	}


	public void SetSpeedInfo (float speed)
	{
		this.speedInfo = speed;
	}
}



