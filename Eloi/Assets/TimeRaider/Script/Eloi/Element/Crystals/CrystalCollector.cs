﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrystalCollector : MonoBehaviour , I_CrystalCollector{


	
	public LinkedList<I_CrystalCollectorListener> listeners = new LinkedList<I_CrystalCollectorListener> ();

	
	public void AddListener (I_CrystalCollectorListener listener)
	{
		if(listener!= null)
			listeners.AddFirst (listener);
	}
	
	
	public void ClearListener (){
		listeners.Clear ();
		
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		Crystal crystal = col.gameObject.GetComponent<Crystal>() as Crystal;
		if( crystal!=null )
		{
		
			foreach( I_CrystalCollectorListener l in listeners)
			{
				l.CrystalRecolted(crystal.value);
			}
			col.gameObject.SetActive(false);
			Destroy(col.gameObject);
		}
	}



}
