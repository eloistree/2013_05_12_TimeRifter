﻿using UnityEngine;
using System.Collections;

public class CheckPointImpl : CheckPoint {

	public GameObject notUsed;
	public GameObject used;

	public bool noWarning ;

	public string TagSensitif = "Player";



	void Start () {
		
		if (notUsed == null) {
			throw new UnityException("The game object when repesenting the check point when not used  is not define");		
		}
		
		if (notUsed == null) {
			throw new UnityException("The game object when repesenting the check point when used used  is not define");		
		}

		if (!noWarning && reappearancePoint.position.Equals( Vector2.zero)) {
			Debug.LogWarning("Is it normal that your reappearance point is not set ?");
		}
	}
	
	public override void SetState (CheckPointState value)
	{
		base.SetState (value);

		switch (value) {
			case CheckPointState.Hide:  	notUsed.SetActive(false);	used.SetActive(false); break;
			case CheckPointState.Enable: 	notUsed.SetActive(true); 	used.SetActive(false); break;
			case CheckPointState.Checked: 	notUsed.SetActive(false);	used.SetActive(true); break;
		}

	}


	public void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.CompareTag(TagSensitif))
			if (CheckPointState.Enable.Equals (state)) {
				SetState(CheckPointState.Checked);
			}
	}

}
