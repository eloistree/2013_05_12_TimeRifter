﻿using UnityEngine;
using System.Collections;

public interface ICheckPointListener  {

	void SwitchState(CheckPointState newOne, CheckPointState oldOne );

}
