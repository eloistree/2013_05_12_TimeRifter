﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CheckPoint : MonoBehaviour, ICheckPoint {

	public LinkedList<ICheckPointListener> listeners = new LinkedList<ICheckPointListener> ();

	public CheckPointState state = CheckPointState.Enable;
	public Transform reappearancePoint;

	public void AddListener (ICheckPointListener listener)
	{
		if(listener!= null)
		listeners.AddFirst (listener);
	}


	public void ClearListener (){
		listeners.Clear ();

	}
	public virtual void SetState (CheckPointState value)
	{
		if (!state.Equals (value)) {
			CheckPointState old = state;
			state=value;
			foreach(ICheckPointListener l in listeners)
			{
				l.SwitchState(state,old);
			}
		}
	}

	public Vector2 GetRepopPosition ()
	{
		return reappearancePoint.position;
	}
}


