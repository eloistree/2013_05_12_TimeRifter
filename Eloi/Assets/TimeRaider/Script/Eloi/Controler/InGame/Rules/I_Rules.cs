﻿using UnityEngine;
using System.Collections;

public interface I_Rules  {

	
	void AddListener(I_RulesListener listener);
	void ClearListener();

	
	int GetMaxLife();
	int GetStartLife();
	
	bool IsAllowToSlide(float time, float lastslide);
	bool IsAllowToJump(float time, float lastjump);
	bool IsAllowToDashToTheGround(float time, float lastjump);
	
	bool IsAllowToJumpBack(float time, float lastjump);
	bool IsAllowToDash(float time, float lastjump);
}
