﻿using UnityEngine;
using System.Collections;

public class RulesImpl : Rules, Refreshable {

	
	public Refresher.RefreshType refreshType = Refresher.RefreshType.EachSecond;
	public Refresher.RefreshType GetRefreshType ()
	{	return refreshType;  }

	public void Refresh (float time)
	{
		// Check if the life are not over the max life
		/// Concillor-> SetLife(maxlife);

		// check if the player is not out of the map
		bool outOfMap = IsPlayerOutOfTheMap();
		//Concillor -> player is death
		if(outOfMap){
			//print (listeners.Count);
			foreach(I_RulesListener rl in listeners)
			{
				rl.PlayerOutOfTheMap();
			}
		}
		
		//Check if the player is death
		bool isOutLife = IsPlayerOutOfLife();
		if(isOutLife){
			foreach(I_RulesListener rl in listeners)
			{
				rl.PlayerOutOfLife();
			}
		}
		//Concillor -> player is desth



		//si le personne side plus de x temps
		// le refaire courir
		if(IsSlidingFinished(time))
		{
			foreach(I_RulesListener rl in listeners)
			{
				rl.StopSliding( time);
			}
		}
	}



	private bool IsSlidingFinished(float time)
	{
		I_InGameData d = GetGameData();
		if(d!=null)
		{
			return time-d.GetLastSlide()> maxSlideTime;
		}
		return false;
	}

	
	private bool IsPlayerOutOfTheMap()
	{
		I_Player p = GetPlayer();
		I_MapSize m = GetMapSize();
		
		if(m!=null && p!=null)
		{
			return m.IsOutOfTheMap(p.GetPosition());
		}
		
		return false;
	}
	

	private bool IsPlayerOutOfLife()
	{
		I_InGameData d = GetGameData();
		if(d!=null)
		{
			int life = d.GetLife();
			print ("l:"+life);
			if(life <=0) return true;
		}
		return false;
	}










	private I_InGameData data ;
	private I_InGameData GetGameData()
	{	
		if(data==null)
		{
			data = InGamePartnerInterface.data;
			
		}
		return data; }



	private I_MapSize mapSize ;
	private I_MapSize GetMapSize()
	{	
		if(mapSize==null)
		{
			mapSize = InGamePartnerInterface.mapSize;
			
		}
		return mapSize; }
	
	private I_Player player ;
	private I_Player GetPlayer()
	{	
		if(player==null)
		{
			player = InGamePartnerInterface.player;
			
		}
		return player; }
	


}
