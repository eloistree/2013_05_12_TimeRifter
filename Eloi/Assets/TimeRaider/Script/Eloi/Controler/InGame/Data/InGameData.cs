﻿using UnityEngine;
using System.Collections;

public class InGameData : I_InGameData {
	private ICheckPoint lastCheckPoint;
	private int crystal ;
	private int life =1;
	private float lastTimeJump;
	private float lastTimeSlide;
	private float lastTimeGroundDash;

	private float lastTimeJumpBack;
	private float lastTimeDash;

	private bool pause;
	
	public void SetCrystal (int crystal)
	{
		this.crystal = crystal;
	}
	
	public int GetCrystal ()
	{
		return crystal;
	}
	public void SaveLastCheckPoint (ICheckPoint cp)
	{
		if(cp!=null)
			lastCheckPoint = cp;
	}
	
	public ICheckPoint GetLastCheckPoint ()
	{
		return lastCheckPoint;
	}
	

	
	public void SetLife(int value)
	{
		if (value<0) value=0;
		life = value;
	}
	public int GetLife()
	{
		return life;
	}
	
	public void SetLastJump (float time)
	{
		lastTimeJump=time;
	}

	public float GetLastSlide ()
	{
		return lastTimeSlide;
	}
	
	public float GetLastJump ()
	{
		return lastTimeJump;
	}
	
	public void SetLastSlide (float time)
	{
		lastTimeSlide=time;
	}

	
	public float GetLastGroundDash ()
	{
		return lastTimeGroundDash;
	}
	
	public void SetLastGroundDash (float time)
	{
		lastTimeGroundDash=time;
	}

	public void SetLastJumpBack (float time)
	{
		 lastTimeJumpBack= time;
	}

	public float GetLastJumpBack ()
	{
		return lastTimeJumpBack;
	}

	public void SetLastDash (float time)
	{
		lastTimeDash = time;
	}

	public float GetLastDash ()
	{
		return lastTimeDash;
	}

	public void SetPause (bool onOff)
	{
		pause = onOff;
	}

	public bool GetPause ()
	{
		return pause;
	}
	public void Reset ()
	{
		lastCheckPoint = null;
		crystal = 0;
	}
}

