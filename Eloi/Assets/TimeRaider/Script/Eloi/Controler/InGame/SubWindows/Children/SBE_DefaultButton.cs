﻿using UnityEngine;
using System.Collections;

public class SBE_DefaultButton : SBE_Button {

	public SubWindowButton returnOnClick  = SubWindowButton.Close;
	
	protected void Awake()
	{
		base.Awake();
		SetWhatToDoOnClick(Answer);
	}
	protected void OnGUI()
	{
		base.OnGUI ();
	}
	
	protected void Answer()
	{
		Time.timeScale=1;
		
		fatherInterface.OnButtonDown(returnOnClick);

	}
}
