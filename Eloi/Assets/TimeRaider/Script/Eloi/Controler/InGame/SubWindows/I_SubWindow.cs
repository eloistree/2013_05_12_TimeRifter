﻿using UnityEngine;
using System.Collections;

public interface I_SubWindow {

	void AddListener(I_SubWindowListener listener);

	/**Order to hide or display the window*/
	void Display(bool onOff);

	/**Ask the window to display and destory*/
	void Remove();


	/** Is call if a button is activated*/
	void OnButtonDown(SubWindowButton button);
	
	/** Is call if Ask to go at a menu is requested*/
	void OnMenuRequested(SubWindowMenuRequest button);
	
	/** Is call if Ask to go at a other submenu is requested*/
	void OnSubMenuRequested(SubWindowSubMenuRequest button);

	void AddChildren(I_SubWindowChildren children, GameObject childrenGameObject);

}
	public enum SubWindowButton { Close, Cancel,Yes, No,Next, Validate}
	public enum SubWindowMenuRequest { MainMenu, Option, Volume, Language, Config, Credit  }
	public enum SubWindowSubMenuRequest { Pause, Option, Volume , None}


