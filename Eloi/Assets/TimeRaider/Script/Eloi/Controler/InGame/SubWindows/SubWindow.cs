﻿using UnityEngine;
using System.Collections.Generic;

public abstract class SubWindow : ZoneElement, I_SubWindow {


	private LinkedList<I_SubWindowListener> listeners = new LinkedList<I_SubWindowListener> ();
	public void AddListener (I_SubWindowListener listener)
	{
		if (listener != null) {
			listeners.AddFirst(listener);	
		}
	}

	public GameObject [] childrenAutoTab = new GameObject[10];
	public LinkedList<I_SubWindowChildren>  childrenList = new  LinkedList<I_SubWindowChildren> ();

	public bool withPause=true;
	public int guiDepth =-100;


	protected void Awake(){
		if (withPause) {
			//TO REMPLACE : it is to the listener to decide what to do on pause, but as I has not make one currently...
			Time.timeScale = 0;
		}
	}




	public void Remove ()
	{
		foreach (GameObject o in childrenAutoTab)
						DestroyImmediate (o);
		foreach (I_SubWindowListener l in listeners)
						l.IsDestroy (this);
		DestroyImmediate (this.gameObject);
	}
	

	public void Display (bool onOff)
	{
		this.gameObject.SetActive (onOff);
	}

	public virtual  void OnButtonDown (SubWindowButton button)
	{


		foreach (I_SubWindowListener l in listeners) {
			l.UseDefaultButton(this,button);		
		}
	
	}

	public virtual  void OnMenuRequested (SubWindowMenuRequest menu)
	{
		foreach (I_SubWindowListener l in listeners) {
			l.AskToGoAt(this,menu);		
		}
	}

	public virtual  void OnSubMenuRequested (SubWindowSubMenuRequest submenu)
	{	
		foreach (I_SubWindowListener l in listeners) {
			l.AskToGoAt(this,submenu,true);		
		}
	}

	public void AddChildren (I_SubWindowChildren iChildren, GameObject oChildren)
	{
		bool added = false;

		for (int i =0; i<childrenAutoTab.Length && !added ; i++)
		{
			if(childrenAutoTab[i]==null)
			{
				childrenAutoTab[i]= oChildren;
				childrenList.AddLast(iChildren);
				added=true;
			}
			if(childrenAutoTab[i]== oChildren)
			{
				added=true; break;
			}
		}

		if (!added) {
			throw new UnityException("Please do a bigger familly ! Make love not war ;)  :"+this.gameObject); 	
		}

	}


}
