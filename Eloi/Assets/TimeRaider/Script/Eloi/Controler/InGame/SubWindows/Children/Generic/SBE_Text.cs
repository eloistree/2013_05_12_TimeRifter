﻿using UnityEngine;
using System.Collections;

public class SBE_Text : SubWindowChildrenElemnt {

	public GUIStyle  textStyle;
	public string text;

	public void Awake(){
		base.Awake ();
	}
	
	protected void OnGUI()
	{
		base.OnGUI ();
		GUI.Label (finalLocalisation, text,textStyle);
	}
	public  override void RefreshPositionAndSize()
	{base.RefreshPositionAndSize();}
}
