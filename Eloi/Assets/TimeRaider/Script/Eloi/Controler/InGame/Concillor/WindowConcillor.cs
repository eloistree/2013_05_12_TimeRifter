
using System;
using System.Collections.Generic;
using UnityEngine;
public class WindowConcillor: Concillor, I_SubWindowListener
{
	private I_SubWindow actualWindows;
	public WindowConcillor (IGameAbilities game) : base(game){	}
	//Stack windows
//	private LinkedList<I_SubWindow> stackWindows = new LinkedList<I_SubWindow> ();
//	protected void AddToTheStack(I_SubWindow win ){
//		stackWindows.AddFirst (win);
//	}
//	protected I_SubWindow RemoveFromTheStack( ){
//		I_SubWindow win;
//		win = stackWindows.First;
//		stackWindows.RemoveFirst ();
//		return win;
//	}
//
//	protected bool RemoveFromTheStack(I_SubWindow win ){
//		return stackWindows.Remove (win);
//	}
	



	public void IsDisplay (I_SubWindow window, bool onOff)
	{
//		if (onOff) {
//			AddToTheStack (window);	
//			game.SetPause(true);	
//		} else  {
//			RemoveFromTheStack(window);		
//			if( stackWindows.Count<=0) 
//				game.SetPause(false);	
//		}

				if (onOff && window != null) {
					game.SetPause(true);
					if (actualWindows != null) {
							actualWindows.Remove ();
							actualWindows = window;

					}		
				} else if (!onOff && window != null) {
					if(window== actualWindows)
					{
				
						actualWindows.Remove ();
						actualWindows =null;
						

					}

				}
	}

	public void Clear()
	{
		//delete all in stack and actual
		if (actualWindows != null) {
						actualWindows.Remove ();
						actualWindows = null;
		}
	}

	public void IsDestroy (I_SubWindow window)
	{
		
		//RemoveFromTheStack(window);	
	}

	public void UseDefaultButton (I_SubWindow window, SubWindowButton button)
	{
		switch (button) {
		case SubWindowButton.Close: 
			Clear();	
			break;
		case SubWindowButton.Next: 
			Clear();		
			game.SetPause(false);
			break;
		
		}
	}

	public void AskToGoAt (I_SubWindow window, SubWindowMenuRequest request)
	{
		switch (request) {
			case SubWindowMenuRequest.MainMenu: 
			
				game.GoTo(InGameToMenu.MainMenu);
			break;
		
		}
	}

	public void AskToGoAt (I_SubWindow window, SubWindowSubMenuRequest request, bool withDestroy)
	{
		throw new NotImplementedException ();
	}
}

