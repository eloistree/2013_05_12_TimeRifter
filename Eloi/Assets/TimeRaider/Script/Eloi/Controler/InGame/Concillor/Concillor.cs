﻿using UnityEngine;
using System.Collections;

/**The role of the concillor is to listen some part of the ingame elements 
 * and to report the message to the correct fonction available by the IGameAbility*/
public abstract class Concillor  {

	/**The concillor can ask to the game to react at the current "event" by a call at the method to an ability provided*/
	protected IGameAbilities game; 


	public Concillor (IGameAbilities game)
	{
		if(game==null){

			throw new UnityException("The reference to InGame class is required :"+ game);
		}
		this.game = game;
	}

}
