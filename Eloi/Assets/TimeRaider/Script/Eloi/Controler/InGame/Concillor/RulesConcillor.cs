﻿using UnityEngine;
using System.Collections;

public class RulesConcillor : Concillor, I_RulesListener {
	
	public RulesConcillor (IGameAbilities game) : base(game){}



	public void PlayerOutOfTheMap ()
	{
		game.SetPlayerDeath(true);
	}

	public void PlayerOutOfLife ()
	{
		
		game.SetPlayerDeath(true);
	}
	public void  StopSliding(float time)
	{
		game.Do(Actions.None,time);
	}

}
