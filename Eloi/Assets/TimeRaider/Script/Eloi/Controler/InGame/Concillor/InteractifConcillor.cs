﻿using UnityEngine;
using System.Collections;

public class InteractifConcillor : Concillor, I_CrystalCollectorListener {
	
	
	public InteractifConcillor (IGameAbilities game) : base(game){}

	

	public void CrystalRecolted (int value)
	{
		game.AddCrystalsRecolted (value);
	}
}
