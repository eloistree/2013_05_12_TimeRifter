﻿using UnityEngine;
using System.Collections;

public class PlayerConcillor : Concillor , I_PlayerListener , Refreshable{
	
	
	public PlayerConcillor (IGameAbilities game) : base(game){}


	public void PlayerDommaged (int value)
	{
		game.ChangePlayerLife(value, true);
	}

	public void PlayerDeath ()
	{
		
		game.SetPlayerDeath(true);
	}

	public void PlayerIsAffectedBy (PlayerBonusMalus value)
	{
		game.AffectThePlayerBy( value);
	}


	public void Refresh (float time)
	{
		I_Player p = InGamePartnerInterface.player;
		I_Slider s = InGamePartnerInterface.slider;
		if(p!=null && s !=null)
		{
			p.SetSpeedInfo( s.GetSpeed ());
		}
	}

	public Refresher.RefreshType GetRefreshType ()
	{
		return Refresher.RefreshType.Often;
	}
}
