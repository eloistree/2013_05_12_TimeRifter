﻿using UnityEngine;
using System.Collections;

public class InputConcillor : Concillor, I_InputListener {


	public InputConcillor (IGameAbilities game) : base(game){

	}

	public void InputEvent (InputPossibility action, float timeHappened)
	{

		switch(action)
		{
			case InputPossibility.BackJump:game.Do(Actions.JumpBack,timeHappened); break;
			case InputPossibility.Dash:game.Do(Actions.Dash,timeHappened);break;
			case InputPossibility.Slide:game.Do(Actions.Slide,timeHappened);break;

			case InputPossibility.GoToGround:game.Do(Actions.GroundDash,timeHappened);break;
			case InputPossibility.Jump:game.Do(Actions.Jump,timeHappened);break;


			case InputPossibility.SlowDown:game.ChangePlayerSpeed(-1f);break;
			case InputPossibility.SpeedUp:game.ChangePlayerSpeed(1f);break;

			case InputPossibility.Teleport: game.Do (Actions.Teleport, timeHappened);break;

			case InputPossibility.Pause:game.Display(InGameSubMenu.Pause) ;break;
			case InputPossibility.Menu:game.Display(InGameSubMenu.Menu);break;
			case InputPossibility.Option:game.Display(InGameSubMenu.Option);break;


		}
	}

	public void InputEvent (InputPossibility action, float timeHappened, Vector2 direction)
	{
		switch(action)
		{
		case InputPossibility.Jump:game.Do(Actions.Jump,timeHappened, direction);break;
			

			
			
		}
	}

	public void SetMovementDirection (Vector2 direction)
	{
		SetMovementDirection(direction, false);
	}

	public void SetMovementDirection (Vector2 direction, bool normelized)
	{
		game.ChangePlayerSpeed(direction.x, true);
	}


}
