

using UnityEngine;
using System;

public class SceneFinder : MonoBehaviour 
{
	private static SceneFinder INSTANCE ;
	public static SceneFinder GetInstance(){return INSTANCE;}


	void Awake()
	{
		INSTANCE= this;
		
		InGamePartnerInterface.ingame = GetInGame();

		InGamePartnerInterface.inputs = GetInputs();
		InGamePartnerInterface.action = GetAction();
		InGamePartnerInterface.checkPoints = GetCheckPoints();
		InGamePartnerInterface.slider = GetSlider();
		InGamePartnerInterface.player = GetPlayer();
		
		InGamePartnerInterface.hud = GetHud();
		
		InGamePartnerInterface.boost = GetBoost();
		InGamePartnerInterface.crystalCollector = GetCrystalCollector();
		
		InGamePartnerInterface	.rules = GetRules();
		InGamePartnerInterface	.mapSize = GetMapSize();
		InGamePartnerInterface	.windowMaster = GetWindowMaster();

		Destroy(this.gameObject);
	}



	public IGameAbilities GetInGame()
	{
		return GameObject.FindObjectOfType<InGameMockImpl>() as InGameMockImpl;
	}


	public I_Input [] GetInputs()
	{
		return GameObject.FindObjectsOfType<InputBasic>() as InputBasic [];
	}


	public  I_Action  GetAction()
	{
		return GameObject.FindObjectOfType<Action>() as Action ;
	}

	public  ICheckPoint [] GetCheckPoints()
	{
		return GameObject.FindObjectsOfType<CheckPoint>() as CheckPoint [];
	}

	public  I_Slider  GetSlider()
	{
		return GameObject.FindObjectOfType<Slider>() as Slider ;
	}

	public  I_Player GetPlayer()
	{
		return GameObject.FindObjectOfType<Player>() as Player ;
	}

	public  I_InGameHud GetHud()
	{
		return GameObject.FindObjectOfType<InGameHud>() as InGameHud ;
	}

	public  IBoostButton GetBoost()
	{
		return GameObject.FindObjectOfType<BoostButton>() as BoostButton ;
	}

	public I_CrystalCollector GetCrystalCollector()
	{
		return GameObject.FindObjectOfType<CrystalCollector>() as CrystalCollector;
	}
	public I_Rules GetRules()
	{
		return GameObject.FindObjectOfType<Rules>() as Rules;
	}
	public I_MapSize GetMapSize()
	{
		return GameObject.FindObjectOfType<MapSize>() as MapSize;
	}

	public IWindowsMaster GetWindowMaster()
	{
		return GameObject.FindObjectOfType<WindowsMaster>() as WindowsMaster;
	}


}
