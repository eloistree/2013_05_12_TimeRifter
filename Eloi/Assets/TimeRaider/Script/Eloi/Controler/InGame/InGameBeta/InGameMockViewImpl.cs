using System;
using UnityEngine;

/**Manage the hud, sound, ... implementation code*/
public class InGameMockViewImpl: InGameMockImpl, Refreshable
{

	public Refresher.RefreshType refreshType = Refresher.RefreshType.Often;





	public void Refresh (float time)
	{
		RefreshMeterRan();
		
		if(Input.GetKey(KeyCode.Keypad0)) ChangePlayerLife(1,true);
		if(Input.GetKey(KeyCode.Keypad1)) ChangePlayerLife(1,false);
	}


	// Warning, technicly, this should be at MapConcillor to change the meter ran and  the methode is going to be override by this clas
	// as map is not build yes, i wrote this code here.
	private void  RefreshMeterRan()
	{
		
		I_InGameHud hud = InGamePartnerInterface.hud;
		I_MapSize mapsize = InGamePartnerInterface.mapSize;
		I_Player player = InGamePartnerInterface.player;
		if(hud!=null && mapsize!=null && player!=null)
		{
			float dist = mapsize.GetDistanceFromStartInMeter(player.GetPosition());

			hud.SetMeters((int)dist);
		}

	}



	public Refresher.RefreshType GetRefreshType ()
	{
		return refreshType;
	}



	public override void ChangePlayerLife (int value, bool decrease)
	{
		base.ChangePlayerLife(value,decrease);

		//Refresh HUD
		I_InGameHud hud = InGamePartnerInterface.hud;
		if(hud!=null)
		{
			hud.SetLife(data.GetLife());
		}
	}

	public override void AddCrystalsRecolted (int value)
	{
		base.AddCrystalsRecolted(value);
		
		//Refresh HUD
		I_InGameHud hud = InGamePartnerInterface.hud;
		if(hud!=null)
		{
			hud.SetCrystals(data.GetCrystal());
		}
	}
	
}


