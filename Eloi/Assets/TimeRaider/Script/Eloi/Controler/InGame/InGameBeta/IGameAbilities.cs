﻿using UnityEngine;
using System.Collections;

public interface IGameAbilities {

	
	void Do(Actions actionType, float time);
	
	void Do(Actions actionType, float time, Vector2 direction);
	/**Set the speed base on constrain of the ISlider linked:  Slow<-1<0 Normal <1 Quick */
	void ChangePlayerSpeed(float power);
	void ChangePlayerSpeed (float power, bool normelise);


	void ChangePlayerLife(int value, bool decrease);

	void AffectThePlayerBy (PlayerBonusMalus value);

	void SetPlayerDeath(bool death);
	void AddCrystalsRecolted( int value);



	void SetNewCheckPoint( ICheckPoint checkPoint);


	void SetPlayerDirection(Vector2 direction,bool normelized);


	void GoTo (InGameToMenu MenuType);
	void Display (InGameSubMenu subMenuType);
	
	void SetPause (bool onoff);
	void Save ();
	void GameWin();

}
