using UnityEngine;
using System;

public  class InGameMockImpl : InGameMock, IGameAbilities
{

	protected I_InGameData data = new InGameData();

	
	protected  new void Start()
	{
		base.Start();
		InGamePartnerInterface.data = data;

		if(rules!=null)
		{
			ChangePlayerLife(rules.GetStartLife());
		}

		SetPause (false);

	}


	protected void Update()
	{
		// have to be in HUD, will cod this after the break
		IBoostButton bo = InGamePartnerInterface.boost;
		if (bo != null) {
						bo.SetPercentLoad (bo.GetPercentLoad () + 0.04f * Time.deltaTime);
		}
	}

	public override void Do (Actions actionType, float time)
	{
	
		Do (actionType, time, Vector2.zero);
	}

	
	public override void Do (Actions actionType, float time, Vector2 direction)
	{
		//If there is a direction, then check if the action correspond at the direction
		if(direction!= Vector2.zero)
		switch(actionType)
		{
			case Actions.Jump:
			case Actions.Slide:
			case Actions.GroundDash:
			if(direction.y<0f && Math.Abs(direction.x)<0.5f) actionType=Actions.GroundDash;
			else if(direction.y>0f && Math.Abs(direction.x)<0.5f) actionType=Actions.Jump;
			else if(direction.x<0f && Math.Abs(direction.y)<0.5f) actionType=Actions.JumpBack;
			else if(direction.x>0f &&  direction.y<0.5f && direction.y>-0.2f ) actionType=Actions.Dash;
			else if(direction.x>0f &&  direction.y<0.2f && direction.y>-0.5f ) actionType=Actions.Slide;
			break;
						
		}

		// Do the action is allow
		I_Rules rules = InGamePartnerInterface.rules;
		if(rules!=null){
			bool actionvalide = true;
			switch(actionType)
			{
				case Actions.Jump:
					actionvalide = rules.IsAllowToJump(time, data.GetLastJump()) ;
				if(actionvalide) data.SetLastJump(time);
						break;
				case Actions.Slide:
					actionvalide = rules.IsAllowToSlide(time, data.GetLastSlide()) ;
				if(actionvalide) data.SetLastSlide(time);
						break;
				case Actions.GroundDash:
					actionvalide = rules.IsAllowToDashToTheGround(time, data.GetLastGroundDash()) ;
				if(actionvalide) data.SetLastGroundDash(time);
						break;
				
			case Actions.JumpBack:
				actionvalide = rules.IsAllowToJumpBack(time, data.GetLastJumpBack()) ;
				if(actionvalide) data.SetLastJumpBack(time);
				break;
				
			case Actions.Dash:
				actionvalide = rules.IsAllowToDash(time, data.GetLastDash()) ;
				if(actionvalide) data.SetLastDash(time);
				break;

			}
			if(!actionvalide)return;
		}


		bool actionDone = false;
		I_Action a = InGamePartnerInterface.action;
		if (a != null) {
			if(direction.Equals(Vector2.zero))
				a.DoThisAction(actionType);
			else
				a.DoThisAction(direction,actionType);
			actionDone=true;
		}
		if(actionDone){
			I_Player p = InGamePartnerInterface.player;
			if (p != null) {
				
				PlayerState ps =PlayerState.Running;
				switch(actionType)
				{
				case Actions.Jump: 
					ps = PlayerState.Jumping;
					data.SetLastJump(time);
					break;
				case Actions.Dash:case Actions.Slide:
					ps = PlayerState.Sliding;
					data.SetLastSlide(time);
					break;
				case Actions.JumpBack:
					ps = PlayerState.JumpingBack;
					break;
					
				case Actions.GroundDash:
					ps = PlayerState.Sliding;
					break;
					
				}
				
				p.SetState(ps);		
			}
			
			
		}
	}
	public override void ChangePlayerSpeed (float power)
	{
		ChangePlayerSpeed(power, false);
	}

	public override void ChangePlayerSpeed (float power, bool normelize)
	{

		I_Slider s = InGamePartnerInterface.slider;
		if (s != null) {
			if(!normelize){
				float val = (s.GetMinSpeed()+s.GetMaxSpeed())/2f;
				s.AffectSpeedBy(val*power,SliderAffectation.Request);
			}
			else
			{
				float normal =s.GetNormalSpeed();
				float val =normal;

				if (power<0)
				{
					float min = s.GetMinSpeed();
					val = normal+(power*(normal-min));
					s.SetSpeed(val, SliderAffectation.FullControl);
				}
				else if (power>0)
				{
					float max = s.GetMaxSpeed();
					val= normal+(power*(max-normal));
					s.SetSpeed(val, SliderAffectation.FullControl);
				}
				else s.SetSpeed(val, SliderAffectation.FullControl);


			}
		}
	}

	public  void ChangePlayerLife (int value)
	{

		data.SetLife(0);
		ChangePlayerLife(value, false);
		
		
	}
	public override void ChangePlayerLife (int value, bool decrease)
	{

		Debug.Log("> Life " + (decrease?"-":"+") + " "+ value);
		int life = data.GetLife();
		int maxlife =rules.GetMaxLife();

		life += decrease?-value:value;
		if(life>maxlife)life=maxlife;
		data.SetLife(life);

	
	}
	
	public override void AddCrystalsRecolted (int value)
	{
		
	//	Debug.Log("> Add crystals: " + value);
		data.SetCrystal (data.GetCrystal () + value);


		// have to be in HUD, will cod this after the break
	
		IBoostButton bo = InGamePartnerInterface.boost;
		bo.SetPercentLoad( bo.GetPercentLoad()+0.04f);
	}
	
	public override void SetNewCheckPoint (ICheckPoint checkPoint)
	{
		
		Debug.Log(">New Check Point " + checkPoint);
		data.SaveLastCheckPoint (checkPoint);

	}
	
	public override void GoTo (InGameToMenu menuType)
	{
		Debug.Log("> Switch menu: " +menuType);

		//Save data
		//Create a persistant data to pass into the Stephane level and communicate whith menu to load
		switch (menuType) {
		case InGameToMenu.MainMenu:		
			Application.LoadLevel ("menu");break;
		
		}

	}
	
	public override void Display (InGameSubMenu subMenuType)
	{
		
		Debug.Log("> Display sub-menu: " +subMenuType);
		//Application.LoadLevel ("menu");
		IWindowsMaster wm = WindowsMaster.GetInstance ();
		if(wm!=null)
		{
			switch (subMenuType) {
			case InGameSubMenu.Menu:		
				wm.DisplayMenuPause(); break;
				
			case InGameSubMenu.Pause:
				wm.DisplayMenuPause(); break;

			case InGameSubMenu.Victory:
				wm.DisplayVictory(); break;
			case InGameSubMenu.Defeat:
				wm.DisplayDefeat(); break;

			}

		}
	}


	public override void SetPlayerDeath (bool death)
	{
		
		Debug.Log("> Player is death: " +death);
		I_Player p = InGamePartnerInterface.player;
		if (p != null) {
			p.SetState(PlayerState.Death);		
		}
		
		I_Slider s = InGamePartnerInterface.slider;
		if (s != null) {
			s.StopRunning(true,1000f);
		}

		Display (InGameSubMenu.Defeat);
						
	
	}
	public override void GameWin(){
		Display (InGameSubMenu.Victory);
		SetPause (true);
	
	}



	public override void AffectThePlayerBy (PlayerBonusMalus value)
	{




	}


	public override void Save ()
	{}

	public override void SetPlayerDirection (Vector2 direction, bool normelized)
	{
	}

	
	public override void SetPause (bool onoff)
	{
		data.SetPause (onoff);
		Debug.Log ("<<<Pause: " + onoff);
		Time.timeScale = onoff ? 0f : 1f;
	}


}
