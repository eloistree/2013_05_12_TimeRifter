
using System;
using UnityEngine;
using System.Collections.Generic;


public class WindowsMaster: MonoBehaviour, IWindowsMaster, I_SubWindowListener
{

	private static WindowsMaster INSTANCE; 
	public static IWindowsMaster GetInstance(){
		return INSTANCE;
	}
	public void Awake(){
		if (INSTANCE == null)
				INSTANCE = this;
	}




	public GameObject mainmenu;
	public GameObject pause;
	public GameObject option;

	
	public GameObject victory;
	public GameObject defeat;
	
	
	public I_SubWindow mainmenuInstance;
	public I_SubWindow pauseInstance;
	public I_SubWindow optionInstance;
	
	public I_SubWindow victoryInstance;
	public I_SubWindow defeatInstance;


	private LinkedList<I_SubWindowListener> listeners = new LinkedList<I_SubWindowListener> ();
	public void AddListener (I_SubWindowListener listener)
	{
		if (listener != null) {
			listeners.AddFirst(listener);		
		}
	}
	public void ClearListener ()
	{
		listeners.Clear ();
	}
	


	public void Clear(){
		if (mainmenuInstance != null) {
			mainmenuInstance.Remove();
			mainmenuInstance=null;
		}
		if (pauseInstance != null) {
			pauseInstance.Remove();
			pauseInstance=null;
		}
		if (optionInstance != null) {
			optionInstance.Remove();
			optionInstance=null;
		}
		if (victoryInstance != null) {
			victoryInstance.Remove();
			victoryInstance=null;
		}
		if (defeatInstance != null) {
			defeatInstance.Remove();
			defeatInstance=null;
		}
	}


	public void ListenTo(GameObject go ){
		I_SubWindow subList = go.GetComponent(typeof(I_SubWindow)) as I_SubWindow;
		if(subList!=null)
		{
			subList.AddListener(this);
		}
		else Destroy(go);
	}

	public void DisplayMainMenu ()
	{
		if(mainmenu!=null && mainmenuInstance==null){
			GameObject go = GameObject.Instantiate(mainmenu) as GameObject;
			mainmenuInstance = go.GetComponent(typeof(I_SubWindow)) as I_SubWindow;;
			ListenTo(go);

		}
	}

	public void DisplayMenuPause ()
	{
		if(pause!=null && pauseInstance==null){ 
			GameObject go = GameObject.Instantiate(pause)as GameObject;
			pauseInstance = go.GetComponent(typeof(I_SubWindow)) as I_SubWindow;;
			ListenTo(go);
		}
	}

	public void DisplayOption ()
	{
		if(option!=null && optionInstance==null){
			GameObject go = GameObject.Instantiate(option) as GameObject;
			optionInstance = go.GetComponent(typeof(I_SubWindow)) as I_SubWindow;;
			ListenTo(go);
		}
	}

	public void DisplayVictory ()
	{
		if(victory!=null && victoryInstance==null){
			GameObject go = GameObject.Instantiate(victory) as GameObject;
			victoryInstance = go.GetComponent(typeof(I_SubWindow)) as I_SubWindow;;
			ListenTo(go);
		}
	}

	public void DisplayDefeat ()
	{
		if(defeat!=null && defeatInstance==null){
			GameObject go = GameObject.Instantiate(defeat) as GameObject;
			defeatInstance = go.GetComponent(typeof(I_SubWindow)) as I_SubWindow;;
			ListenTo(go);
		}
	}




	//###################### Transmettor


	public void IsDisplay (I_SubWindow window, bool onOff)
	{
		if (listeners != null) {
			foreach(I_SubWindowListener l in listeners)
			{
				if(l!=null)
				l.IsDisplay(window, onOff);
			}		
		}
	}

	public void IsDestroy (I_SubWindow window)
	{
		if (listeners != null) {
			foreach(I_SubWindowListener l in listeners)
			{
				if(l!=null)
					l.IsDestroy(window);
			}		
		}
	}

	public void UseDefaultButton (I_SubWindow window, SubWindowButton button)
	{
		
		if (listeners != null) {
			foreach(I_SubWindowListener l in listeners)
			{
				if(l!=null)
					l.UseDefaultButton(window, button);
			}		
		}
		if(SubWindowButton.Close.Equals(button) || SubWindowButton.Next.Equals(button)  )
		{

			Clear ();
		}
	}



	public void AskToGoAt (I_SubWindow window, SubWindowMenuRequest request)
	{
		
		if (listeners != null) {
			foreach(I_SubWindowListener l in listeners)
			{
				if(l!=null)
					l.AskToGoAt(window, request);
			}		
		}
	}

	public void AskToGoAt (I_SubWindow window, SubWindowSubMenuRequest request, bool withDestroy)
	{
		
		if (listeners != null) {
			foreach(I_SubWindowListener l in listeners)
			{
				if(l!=null)
					l.AskToGoAt(window, request,withDestroy);
			}		
		}

		if (withDestroy)
				window.Remove ();
	}



}
