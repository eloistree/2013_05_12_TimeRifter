

public enum InputPossibility
{
	Jump, BackJump, GoToGround, Dash, Slide, Teleport, SlowDown, SpeedUp, Pause, Menu, Option, Cancel, Valider, Unknown, Yes, No, PowerA, PowerB
}
