﻿using UnityEngine;
using System.Collections;

public interface I_InputListener  {
	
	void InputEvent(InputPossibility action, float timeHappened);
	void InputEvent(InputPossibility action, float timeHappened, Vector2 direction);
	
	void SetMovementDirection( Vector2 direction);
	void SetMovementDirection( Vector2 direction, bool normelized);

}
