﻿using UnityEngine;
using System.Collections;

public class InputMobileClick : InputBasic {


	protected void InputUsed(InputPossibility movetype, float time)
	{
		
		foreach (I_InputListener l in listeners) {
			l.InputEvent(movetype, time);
		}
	}

}
