﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;

public class Menu : MonoBehaviour {

    private int SelectMenu, Campagne, CurrentLanguage, i;
    private float VectorX, VectorY;

    public TextAsset ConfigLanMenuPrin;
    public Texture2D[] Image = new Texture2D[25];
    public Texture2D[] ILanguage = new Texture2D[8];

    public GUIStyle StyleMenu;
    public GUIStyle StyleBouton;
    public GUIStyle StyleLabel;

    public Texture2D FlecheD, FlecheG;

    private string BtnCamp, BtnComp, BtnOptn, BtnExit;
    private string PeriodH, LabPerEG, LabPerMA, LabPerJF, LabPerPR, LabPerFT;
    private string BtnRtrn, BtnEdit, BtnAmis, BtnMulti, BtnLevel;
    private string BtnCred, BtnLang, BtnMusic, BtnJouer;
    private string LabCred1, LabCred2, LabCred3, LabCred4, LabCred5, LabCred6;
    private string NomNiv, ApplicationLevel;

    List<Dictionary<string, string>> MenuPrinL = new List<Dictionary<string, string>>();
    Dictionary<string, string> MenuPrinD;

    void Awake()
    {
        Reader();
    }

	void Start () 
    {
        SelectMenu = 0;
        Campagne = 0;

        CurrentLanguage = 1;

        i = 0;

        VectorX = (Screen.width / 100);
        VectorY = (Screen.height / 100);
	}
	
	
	void Update () 
    {
        MenuPrinL[CurrentLanguage].TryGetValue("BtnCamp", out BtnCamp);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnComp", out BtnComp);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnOptn", out BtnOptn);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnExit", out BtnExit);

        MenuPrinL[CurrentLanguage].TryGetValue("BtnRtrn", out BtnRtrn);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnEdit", out BtnEdit);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnAmis", out BtnAmis);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnMulti", out BtnMulti);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnLevel", out BtnLevel);

        MenuPrinL[CurrentLanguage].TryGetValue("BtnJouer", out BtnJouer);

        MenuPrinL[CurrentLanguage].TryGetValue("BtnCred", out BtnCred);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnLang", out BtnLang);
        MenuPrinL[CurrentLanguage].TryGetValue("BtnMusic", out BtnMusic);

        MenuPrinL[CurrentLanguage].TryGetValue("LabPerEG", out LabPerEG);
        MenuPrinL[CurrentLanguage].TryGetValue("LabPerMA", out LabPerMA);
        MenuPrinL[CurrentLanguage].TryGetValue("LabPerJF", out LabPerJF);
        MenuPrinL[CurrentLanguage].TryGetValue("LabPerPR", out LabPerPR);
        MenuPrinL[CurrentLanguage].TryGetValue("LabPerFT", out LabPerFT);

        MenuPrinL[CurrentLanguage].TryGetValue("LabCred1", out LabCred1);
        MenuPrinL[CurrentLanguage].TryGetValue("LabCred2", out LabCred2);
        MenuPrinL[CurrentLanguage].TryGetValue("LabCred3", out LabCred3);
        MenuPrinL[CurrentLanguage].TryGetValue("LabCred4", out LabCred4);
        MenuPrinL[CurrentLanguage].TryGetValue("LabCred5", out LabCred5);
        MenuPrinL[CurrentLanguage].TryGetValue("LabCred6", out LabCred6);
	}

    void OnGUI()
    {
        switch (SelectMenu)
        {
            case 0: MenuPrincipal();
                break;
            case 1: MenuCampagn();
                break;
            case 2: MenuLeague();
                break;
            case 3: MenuOption();
                break;
            case 4: Credit();
                break;
            case 5: Level();
                break;
            case 6: Music();
                break;
            //case 7: Friend();
            //    break;
            //case 8: OnLine();
            //    break;
            case 9: LevelCreate();
                break;
            case 10: Language();
                break;
        }
    }

    void Reader()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(ConfigLanMenuPrin.text);
        XmlNodeList languagesList = xmlDoc.GetElementsByTagName("language");

        foreach (XmlNode languageValue in languagesList)
        {
            XmlNodeList languageContent = languageValue.ChildNodes;
            MenuPrinD = new Dictionary<string, string>();

            foreach (XmlNode value in languageContent)
            {
                if (value.Name == "BtnCamp")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnComp")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnOptn")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnExit")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnRtrn")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnEdit")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnAmis")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnMulti")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnLevel")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnCred")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnLang")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnMusic")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "BtnJouer")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabPerEG")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabPerMA")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabPerJF")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabPerPR")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabPerFT")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabCred1")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabCred2")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabCred3")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabCred4")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabCred5")
                    MenuPrinD.Add(value.Name, value.InnerText);
                if (value.Name == "LabCred6")
                    MenuPrinD.Add(value.Name, value.InnerText);

            }
            MenuPrinL.Add(MenuPrinD);
        }
    }

    void MenuPrincipal()
    {
        Campagne = 0;
        guiTexture.texture = Image[25];

        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 35 - 50 / 2, 100, 50), BtnCamp, StyleMenu))
        {
            SelectMenu = 1;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 50 - 50 / 2, 100, 50), BtnComp, StyleMenu))
        {
            SelectMenu = 2;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 65 - 50 / 2, 100, 50), BtnOptn, StyleMenu))
        {
            SelectMenu = 3;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 80 - 50 / 2, 100, 50), BtnExit, StyleMenu))
        {
            Application.Quit();
        }
    }

    void MenuCampagn()
    {
        if (Campagne == 0)
        {
            PeriodH = LabPerEG;
            guiTexture.texture = Image[20];
            
            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[0], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "1 - 1";
                ApplicationLevel = "Level_1_1";
                i = 0;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[1], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "1 - 2";
				ApplicationLevel = "Level_P_P";
                i = 1;
            }
            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[2], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "1 - 3";
				ApplicationLevel = "Level_1_2";
                i = 2;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[3], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "1 - 4";
                ApplicationLevel = "";
                i = 3;
            }
            if (GUI.Button(new Rect(VectorX * 90 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheD, StyleMenu))
            {
                Campagne++;
            }
        }
        else if (Campagne == 1)
        {
            guiTexture.texture = Image[21];
            PeriodH = LabPerMA;

            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[4], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "2 - 1";
                ApplicationLevel = "";
                i = 4;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[5], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "2 - 2";
                ApplicationLevel = "";
                i = 5;
            }
            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[6], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "2 - 3";
                ApplicationLevel = "";
                i = 6;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[7], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "2 - 4";
                ApplicationLevel = "";
                i = 7;
            }
            if (GUI.Button(new Rect(VectorX * 10 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheG, StyleMenu))
            {
                Campagne--;

            }
            if (GUI.Button(new Rect(VectorX * 90 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheD, StyleMenu))
            {
                Campagne++;
            }

        }
        else if (Campagne == 2)
        {
            PeriodH = LabPerJF;
            guiTexture.texture = Image[22];

            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[8], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "3 - 1";
                ApplicationLevel = "";
                i = 8;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[9], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "3 - 2";
                ApplicationLevel = "";
                i = 9;
            }
            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[10], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "3 - 3";
                ApplicationLevel = "";
                i = 10;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[11], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "3 - 4";
                ApplicationLevel = "";
                i = 11;
            }
            if (GUI.Button(new Rect(VectorX * 10 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheG, StyleMenu))
            {
                Campagne--;

            }
            if (GUI.Button(new Rect(VectorX * 90 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheD, StyleMenu))
            {
                Campagne++;
            }

        }
        else if (Campagne == 3)
        {
            PeriodH = LabPerPR;
            guiTexture.texture = Image[23];

            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[12], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "4 - 1";
                ApplicationLevel = "";
                i = 12;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[13], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "4 - 2";
                ApplicationLevel = "";
                i = 13;
            }
            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[14], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "4 - 3";
                ApplicationLevel = "";
                i = 14;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[15], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "4 - 4";
                ApplicationLevel = "";
                i = 15;
            }
            if (GUI.Button(new Rect(VectorX * 10 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheG, StyleMenu))
            {
                Campagne--;

            }
            if (GUI.Button(new Rect(VectorX * 90 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheD, StyleMenu))
            {
                Campagne++;
            }

        }
        else if (Campagne == 4)
        {
            PeriodH = LabPerFT;
            guiTexture.texture = Image[24];

            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[16], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "5 - 1";
                ApplicationLevel = "";
                i = 16;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 45 - 100 / 2, 150, 100), Image[17], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "5 - 2";
                ApplicationLevel = "";
                i = 17;
            }
            if (GUI.Button(new Rect(VectorX * 25 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[18], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "5 - 3";
                ApplicationLevel = "";
                i = 18;
            }
            if (GUI.Button(new Rect(VectorX * 75 - 150 / 2, VectorY * 75 - 100 / 2, 150, 100), Image[19], StyleMenu))
            {
                SelectMenu = 5;
                NomNiv = "5 - 4";
                ApplicationLevel = "";
                i = 19;
            }

            if (GUI.Button(new Rect(VectorX * 10 - 100 / 2, VectorY * 15 - 100 / 2, 100, 100), FlecheG, StyleMenu))
            {
                Campagne--;

            }
        }

        GUI.Label(new Rect(VectorX * 55 - 100 / 2, VectorY * 15 - 50 / 2, 100, 50), PeriodH, StyleMenu);

        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 0;
        }
    }

    void MenuLeague()
    {
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 35 - 50 / 2, 100, 50), BtnEdit, StyleMenu))
        {
            //Application.LoadLevel("");
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 50 - 50 / 2, 100, 50), BtnAmis, StyleMenu))
        {
            SelectMenu = 7;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 65 - 50 / 2, 100, 50), BtnMulti, StyleMenu))
        {
            SelectMenu = 8;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 80 - 50 / 2, 100, 50), BtnLevel, StyleMenu))
        {
            SelectMenu = 9;
        }
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 0;
        }
    }

    void MenuOption()
    {
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 50 - 50 / 2, 100, 50), BtnMusic, StyleMenu))
        {
            SelectMenu = 6;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 65 - 50 / 2, 100, 50), BtnLang, StyleMenu))
        {
            SelectMenu = 10;
        }
        if (GUI.Button(new Rect(VectorX * 50 - 100 / 2, VectorY * 80 - 50 / 2, 100, 50), BtnCred, StyleMenu))
        {
            SelectMenu = 4;
        }
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 0;
        }
    }

    void Language()
    {
        if (GUI.Button(new Rect(VectorX * 25 - 250 / 2, VectorY * 50 - 150 / 2, 120, 50), ILanguage[0], StyleMenu))
        {
            CurrentLanguage = 0;
        }
        if (GUI.Button(new Rect(VectorX * 45 - 250 / 2, VectorY * 50 - 150 / 2, 120, 50), ILanguage[1], StyleMenu))
        {
            CurrentLanguage = 1;
        }
        if (GUI.Button(new Rect(VectorX * 65 - 250 / 2, VectorY * 50 - 150 / 2, 120, 50), ILanguage[2], StyleMenu))
        {
            Debug.Log("It");
        }
        if (GUI.Button(new Rect(VectorX * 85 - 250 / 2, VectorY * 50 - 150 / 2, 120, 50), ILanguage[3], StyleMenu))
        {
            Debug.Log("Al");
        }
        if (GUI.Button(new Rect(VectorX * 25 - 250 / 2, VectorY * 75 - 150 / 2, 120, 50), ILanguage[4], StyleMenu))
        {
            Debug.Log("Nd");
        }
        if (GUI.Button(new Rect(VectorX * 45 - 250 / 2, VectorY * 75 - 150 / 2, 120, 50), ILanguage[5], StyleMenu))
        {
            Debug.Log("Es");
        }
        if (GUI.Button(new Rect(VectorX * 65 - 250 / 2, VectorY * 75 - 150 / 2, 120, 50), ILanguage[6], StyleMenu))
        {
            Debug.Log("Pt");
        }
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 3;
        }
    }

    void Level()
    {
        GUI.Label(new Rect(VectorX * 55 - 100 / 2, VectorY * 15 - 50 / 2, 100, 50), PeriodH, StyleMenu);
        GUI.Label(new Rect(VectorX * 55 - 100 / 2, VectorY * 30 - 50 / 2, 100, 50), NomNiv, StyleMenu);

        //GUI.DrawTexture(new Rect(VectorX * 85 - 150 / 2, VectorY * 25 - 100 / 2, 150, 100), Image[i]);

        if (GUI.Button(new Rect(VectorX * 15 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnJouer, StyleBouton))
        {
            Application.LoadLevel(ApplicationLevel);
        }
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 1;
        }
    }

    void Credit()
    {
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 37 - 30 / 2, 100, 30), LabCred1, StyleLabel);
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 44 - 30 / 2, 100, 30), LabCred2, StyleLabel);
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 51 - 30 / 2, 100, 30), LabCred3, StyleLabel);
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 58 - 30 / 2, 100, 30), LabCred4, StyleLabel);
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 65 - 30 / 2, 100, 30), LabCred5, StyleLabel);
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 72 - 30 / 2, 100, 30), LabCred6, StyleLabel);

        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 3;
        }
    }

    void Music()
    {
        GUI.Label(new Rect(VectorX * 35 - 100 / 2, VectorY * 51 - 30 / 2, 100, 30), BtnMusic, StyleLabel);
        audio.volume = GUI.HorizontalSlider(new Rect(VectorX * 48 - 100 / 2, VectorY * 54 - 30 / 2, 200, 50), audio.volume, 0.0F, 1.0F);
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 3;
        }
    }

    void Friend()
    {
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 2;
        }
    }

    void OnLine()
    {
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 2;
        }
    }

    void LevelCreate()
    {
        if (GUI.Button(new Rect(VectorX * 85 - 100 / 2, VectorY * 105 - 50 / 2, 100, 50), BtnRtrn, StyleBouton))
        {
            SelectMenu = 2;
        }
    }
}
