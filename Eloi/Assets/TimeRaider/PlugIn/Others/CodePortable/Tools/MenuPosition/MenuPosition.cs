﻿

public enum MenuPositionType  {

	TopLeftCorner, TopRightCorner, BotLeftCorner, BotRightCorner, Center
}

public enum MenuMesureType
{

	PX, PCT
}