﻿using UnityEngine;
using System.Collections;

public class MenuPositionTool  {
	
	
	public  static  Vector2 GetPositionPointInScreen( MenuPositionType posType)
	{
		return GetPositionPointInRect (posType, MenuMesureType.PX, Screen.width,Screen.height);
		
	}
	public  static  Vector2 GetPositionPointInScreen( MenuPositionType posType,MenuMesureType mesure)
	{
		return GetPositionPointInRect (posType,mesure, Screen.width,Screen.height);
		
	}
	
	public  static  Vector2 GetPositionPointInRect( MenuPositionType posType, MenuMesureType mesure, float width, float height)
	{

		Vector2 v = new Vector2 ();
		float w	= width;
		float h = height;


		switch (posType) {
			case MenuPositionType.TopLeftCorner: 							break;
			case MenuPositionType.TopRightCorner:		v.x=1f;				break;
			case MenuPositionType.BotLeftCorner: 		v.y=1f; 			break;
			case MenuPositionType.BotRightCorner: 		v.x=1f;v.y=1f; 		break;
							default :
			case MenuPositionType.Center: 				v.x=0.5f;v.y=0.5f;	break;
				
		}

		if (MenuMesureType.PX.Equals (mesure)) {
						v.x *= w;
						v.y *= h;
				}
		
		return v;
	}
	
	
	/**
	 * This methode return a rectangle  center on x, y (rect point) regarding the MenuPosition

	 */
	public  static  Rect GetScreenLocalisationOf( MenuPositionType posType,  Rect  zone, bool withCenter)
	{
		
		return GetScreenLocalisationOf(posType, MenuMesureType.PX, zone, withCenter,true,false);
	}
	public  static  Rect GetScreenLocalisationOf( MenuPositionType posType, MenuMesureType mesure,  Rect  zone, bool withCenter, bool onWidth,bool onHeight)
	{
		Rect parent = new Rect (0, 0, Screen.width, Screen.height);
		return GetScreenLocalisationOf (posType, mesure, parent, zone, withCenter, onWidth, onHeight);
	}
	public  static  Rect GetScreenLocalisationOf( MenuPositionType posType, MenuMesureType mesure, Rect parentZone,  Rect  zone, bool withCenter, bool onWidth,bool onHeight)
	{
		bool isInPct = MenuMesureType.PCT==mesure;
		//All in px 
		float w = parentZone.width;
		float h = parentZone.height;
		float x = parentZone.x;
		float y = parentZone.y;

		Rect value =new Rect ();

		//move to the position of the parent
		value.x		= x;
		value.y 	= y;

		//Where  the location where to set the zone in the window on the parent
		Vector2 p = GetPositionPointInRect (posType, mesure, w,h);
		if(isInPct)
		{
			p.x+= p.x*w;
			p.y+= p.y*h;

		}
		value.x +=p.x;
		value.y +=p.y;


		//set the position wanted by the adjustement
		if (isInPct)
		{
			value.x+= w*zone.x;
			value.y+= h*zone.y;
		}
		else
		{
			value.x+=zone.x;
			value.y+=zone.y;
		}

		//center the element if wanted
		if(withCenter)
		{
			if (isInPct)
			{
				value.x-= (zone.width*w)/2f;
				value.y-= (zone.height*h)/2f;
			}
			else
			{
				value.x-= zone.width/2f;
				value.y-= zone.height/2f;
			}
		}

		//set the width and height

		
		float width = zone.width;
		float height = zone.height;
		
		if(isInPct)
		{
	
			if(!onWidth && ! onHeight)
			{
				width= zone.width*w;
				height= zone.height*h;
			}
			if(onWidth)
			{
				width= zone.width*w;
				height= zone.height*w;
				
			}
			
			else 	if(onHeight)
			{
				width= zone.width*h;
				height= zone.height*h;
				
			}
			else
			{
					width= zone.width*w;
					height= zone.height*h;

			}

		}
	
		value.width= width;
		value.height= height;


		return value;
	}
	
	
	public static bool IsPointInZone(float x,float y, Rect zone)
	{
		
		if (y< zone.y) return false;
		if (y> zone.y+zone.height) return false;
		
		if (x< zone.x) return false;
		if (x> zone.x+zone.width) return false;
		
		return true;
	}
	
}
