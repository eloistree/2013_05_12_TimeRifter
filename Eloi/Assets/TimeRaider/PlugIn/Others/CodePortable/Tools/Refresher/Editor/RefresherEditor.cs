
using System;
using UnityEditor;
 		[CustomEditor(typeof(Refresher))]
public class RefresherEditor : Editor
{
				
	public override void OnInspectorGUI()
	{

		base.OnInspectorGUI();
		Refresher r = (Refresher) target;
		EditorGUILayout.LabelField  ("Sometime: "+r.GetLenght(Refresher.RefreshType.Sometime ));
		EditorGUILayout.LabelField("Each Second: "+r.GetLenght(Refresher.RefreshType.EachSecond ));
		EditorGUILayout.LabelField("Often: "+r.GetLenght(Refresher.RefreshType.Often ));
		EditorGUILayout.LabelField("Quick: "+r.GetLenght(Refresher.RefreshType.Quick ));
	}
}


