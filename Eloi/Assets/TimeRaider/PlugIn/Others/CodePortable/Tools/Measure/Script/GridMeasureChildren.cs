﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class GridMeasureChildren : MonoBehaviour {

	public GridMeasureImpl father;

	[HideInInspector]
	public string width ;
	[HideInInspector]
	public string height ;


	void Start()
	{

	}
	// Update is called once per frame
	void Update () {
		if (father == null && this.gameObject.transform.parent !=null) {
			
			father= this.gameObject.transform.parent.GetComponent<GridMeasureImpl>() as GridMeasureImpl;
			if(father==null)
			{
				throw new UnityException("No grid are referenced !!!");
			}
		}
		if(father!=null){
			width = father.width;
			height = father.height;
				
		}
	}
}
