﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GridMeasureChildren))]
public class GridMeasureChildrenEditor : Editor {


	public override void OnInspectorGUI()
	{
		GridMeasureChildren t = (GridMeasureChildren) target;
		if( t!=null){
			EditorGUILayout.HelpBox("Width: "+t.father.width, MessageType.Info);
			EditorGUILayout.HelpBox("Height: "+t.father.height, MessageType.Info);


		}
	}
}
