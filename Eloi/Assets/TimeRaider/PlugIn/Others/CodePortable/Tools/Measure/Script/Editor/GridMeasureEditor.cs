﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GridMeasureImpl))]
public class GridMeasureEditor : Editor {

	float w ;
	float h;







	public override void OnInspectorGUI(){
		base.OnInspectorGUI ();

		GridMeasureImpl grid = (GridMeasureImpl)target;

		if (grid.IsValide ()) {
				
					

			EditorGUILayout.HelpBox("Width: "+grid.width, MessageType.Info);
			EditorGUILayout.HelpBox("Height: "+grid.height, MessageType.Info);


			//Drawing.DrawLine (new Rect(10,10,100,100));
				}
	}
}
