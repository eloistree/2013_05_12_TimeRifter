using System;
using UnityEngine;
	public interface IPowerButton : I_ZoneElement
	{


	
	float GetLastTimeClicked();
	float GetLastTimeActivated();
	bool  HasBeenClickRecently ( float time);
	void Activate();
	bool IsItReady();

	void SetIcon (Texture2D icon);
	}


