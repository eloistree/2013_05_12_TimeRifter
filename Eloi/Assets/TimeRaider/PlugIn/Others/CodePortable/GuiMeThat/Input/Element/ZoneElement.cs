﻿using UnityEngine;
using System.Collections;

public class ZoneElement : GUI_Element, I_ZoneElement , Refreshable{


	public bool zoneSharable=true;
	public bool displayDebugZone =true;

	public Refresher.RefreshType frequenceRefresh= Refresher.RefreshType.Often;

	protected override void Update()
	{
		base.Update();
		if(displayDebugZone)
			DisplayDebugZone();
		
	}
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void OnGUI()
	{
		base.Awake();
	}


	public bool IsInZone (float x, float y)
	{
		return MenuPositionTool.IsPointInZone(x,y, finalLocalisation);
	}
	
	public  bool IsZoneSharable (){return zoneSharable;}
	
	public void DisplayDebugZone ()
	{
		float x =finalLocalisation.x;
		float y =finalLocalisation.y;
		float width = finalLocalisation.width ;
		float height= finalLocalisation.height ;
		
		//		print("hhh");
		Vector2 topLeft = new Vector2(x,-y);
		Vector2 topRight = new Vector2(x+width,-y);
		Vector2 botLeft = new Vector2(x,-(y+height));
		Vector2 botRight = new Vector2(x+width,-(y+height));
		
		Debug.DrawLine(topLeft,topRight, zoneSharable? Color.green:Color.red);
		Debug.DrawLine(topLeft,botLeft, zoneSharable? Color.green:Color.red);
		
		Debug.DrawLine(botRight,topRight, zoneSharable? Color.green:Color.red);
		Debug.DrawLine(botRight,botLeft, zoneSharable? Color.green:Color.red);
		
		
		
		x =0;
		y =0;
		width = Screen.width ;
		height= Screen.height ;
		
		topLeft = new Vector2(x,-y);
		topRight = new Vector2(x+width,-y);
		botLeft = new Vector2(x,-(y+height));
		botRight = new Vector2(x+width,-(y+height));
		
		Debug.DrawLine(topLeft,topRight, Color.yellow);
		Debug.DrawLine(topLeft,botLeft, Color.yellow);
		
		Debug.DrawLine(botRight,topRight, Color.yellow);
		Debug.DrawLine(botRight,botLeft, Color.yellow);
		
		
		
		
		
		
	}
	
	public static  bool IsInAZoneNotShared(float x, float y)
	{

		ZoneElement[] ze = GameObject.FindObjectsOfType<ZoneElement> () as ZoneElement[];
		foreach(ZoneElement z in ze)
		{
			if( z !=null && ! z.IsZoneSharable()){
					if( z.IsInZone(x,y))
				{
					return true;
				}
			}
		}
		return false;
		
	}


	public virtual void Refresh (float time)
	{

	}

	public Refresher.RefreshType GetRefreshType ()
	{
		return frequenceRefresh;
	}


}
