using System;
		public interface I_ZoneElement
		{
			/**Do the x, y coord are in the zone reserved by this object*/
			bool IsInZone(float x, float y);

			/**Do the object accept to share the zone it need*/
			bool IsZoneSharable();
			
			/** Display in the scene view the rectangle of the zone used*/
			void DisplayDebugZone();
		}


