using System;
using UnityEngine;

public  class PowerButtonImpl: PowerButton
{

	public float iconBorder =5f;

	public GUIStyle backGroundButton;
	public Texture2D txtIcon;

	private Rect iconZone = new Rect();


	protected override void Awake()
	{
		base.Awake();

		BorderSizeRefresh();
	}

	protected override void OnGUI()
	{
		
		
		if(GUI.Button(finalLocalisation, "", backGroundButton))
		{

			Clicked();

		}

		if(positionRefresh){
			BorderSizeRefresh();
		}
		if(txtIcon!=null)
		GUI.DrawTexture(iconZone,txtIcon);
		
	}

	void BorderSizeRefresh()
	{
		
		iconZone.x=finalLocalisation.x+iconBorder/2f;
		iconZone.y=finalLocalisation.y+iconBorder/2f;
		iconZone.width=finalLocalisation.width-iconBorder;
		iconZone.height=finalLocalisation.height-iconBorder;

	}

	public override void SetIcon (Texture2D icon)
	{
		this.txtIcon=icon;
	}
}


