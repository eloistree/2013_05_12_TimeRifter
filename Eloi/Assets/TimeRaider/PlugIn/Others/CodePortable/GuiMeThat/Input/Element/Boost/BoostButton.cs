using System;
using UnityEngine;
public abstract class BoostButton  : ZoneElement, IBoostButton 
{
					
	
	public float lastTimeClicked;
	public float lastTimeActivated;

	public float pctCharged;

	public bool isReadyAndNotUse;

	/**It is to the game to decide if the boost has to be activatd, but this bool give the opportunity to test the button when click on it*/
	public bool activateOnClick = true;


	public bool IsItReady()
	{
		if(isReadyAndNotUse) return isReadyAndNotUse;

		bool ok = pctCharged>=1f;
		isReadyAndNotUse=ok;
		return isReadyAndNotUse;
	}

	public void Activate()
	{
		lastTimeClicked= Time.timeSinceLevelLoad;

		if(pctCharged>=1 )
		Reset ();
	}

	public  void Reset()
	{
		lastTimeActivated = Time.timeSinceLevelLoad;
		lastTimeClicked=0;
		pctCharged=0;
		isReadyAndNotUse= false;
	}


	public float GetPercentLoad ()
	{
	
		return pctCharged;
	}

	
	public void  SetPercentLoad ( float value)
	{	

		pctCharged = value;
		if(pctCharged>=1f)
		isReadyAndNotUse=true;

	}



	public float GetLastTimeClicked ()
	{
		return lastTimeClicked;
	}

	public float GetLastTimeActivated ()
	{
		return lastTimeActivated;
	}

	public bool HasBeenClickRecently (float time)
	{
		return (Time.timeSinceLevelLoad-lastTimeClicked)<time;
	}
	
	protected void Clicked(){			
		lastTimeClicked= Time.timeSinceLevelLoad;

	}

	public void Highlight (bool onOff)
	{
		throw new NotImplementedException ();
	}
}

