using System;
using UnityEngine;
public class BoostButtonImpl :BoostButton
{
		

		public Rect chargingZone = new Rect(0,0,200,50);
		public float StartchargingZone;
		public float EndchargingZone;
		public GUIStyle backGroundButton;
		public Texture2D txtChargingZone;
		public Texture2D txtForgroundCharging;
		public Texture2D txtForgroundCharged;


		void OnGUI()
		{

			
		if(GUI.Button(finalLocalisation, "", backGroundButton))
		{
			Clicked();

		}
		if(pctCharged>0){
			Rect display = finalLocalisation;
			display.width= StartchargingZone+ EndchargingZone* pctCharged;

			GUI.BeginGroup(display);

			GUI.DrawTexture(chargingZone,txtChargingZone);
					

			GUI.EndGroup();
		}

			if(pctCharged>=1f)
			GUI.DrawTexture(finalLocalisation,txtForgroundCharged);
			else
			GUI.DrawTexture(finalLocalisation,txtForgroundCharging);

		}


}


