﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class RotatifTrap : TriggeredTrap {

	public float velocity = 100.0f;

	void Update () {
	
		if(ActivatedTrap && block!=null )
			block.transform.Rotate( Vector3.forward*velocity*Time.deltaTime);
	}


	public override void OnCollisionEnterDectected(Collider2D col)
	{
		if (col.gameObject.tag.Equals ("Player")) 
		{
			ApplyEffectTo(col.gameObject);
			
		}
	}
	public override void OnCollisionExitDectected(Collider2D col)
	{
		
	}public override void OnCollisionDectected(Collider2D col)
	{
		
	}
}

