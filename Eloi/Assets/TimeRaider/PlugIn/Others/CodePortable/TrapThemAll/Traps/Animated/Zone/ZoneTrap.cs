﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class ZoneTrap : AnimatedTrap {

	public override void OnCollisionEnterDectected(Collider2D col)
	{
		
		if (col.gameObject.tag.Equals ("Player")) 
		{
			ApplyEffectTo(col.gameObject);
			
		}
	}
	public override void OnCollisionExitDectected(Collision2D col)
	{
		
	}public override void OnCollisionDectected(Collision2D col)
	{
		
	}
}
