﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

/**The block trap represent a trap that is fixed and when triggered will fall (gravity apply)*/
public class BlockTrap : TriggeredTrap {

	public float gravityOnFall =9.81f;

	public new void Start(){
		base.Start ();
		if (block.rigidbody2D == null ) {
			if(!NoWarning)
			Debug.LogWarning ("The trap is have not rigidbody define");
			throw new UnityException("The trap is have not rigidbody define");
		}


	}



	protected   override void OnTrapActivate()
	{
		base.OnTrapActivate ();
		block.SetActive (true);
		block.rigidbody2D.gravityScale=gravityOnFall;
		block.rigidbody2D.isKinematic=false;
		

	}
	public override void OnCollisionEnterDectected(Collision2D col)
	{

		if (col.gameObject.tag.Equals ("Player")) 
		{
			ApplyEffectTo(col.gameObject);
			
		}
	}
	public override void OnCollisionExitDectected(Collision2D col)
	{
		
	}public override void OnCollisionDectected(Collision2D col)
	{
		
	}
}
