﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public class FallingGroundTrap : BlockTrap {

	public enum FallingType {Solid, Impalpable}
	public FallingType fallingType= FallingType.Impalpable;

	/**When a object fall, it usually has a rotation with it*/
	public float rotationEffect=0.5f;


	protected override void OnTrapActivate ()
	{
		base.OnTrapActivate ();
		if( block !=null && block.rigidbody2D==null )
		{
			
			print ("hello rot");
			block.rigidbody2D.AddTorque(rotationEffect);
		}

		if(fallingType.Equals(FallingType.Impalpable) && block !=null && block.collider2D !=null )
		{
			block.collider2D.enabled=false;
		}


	}
}
