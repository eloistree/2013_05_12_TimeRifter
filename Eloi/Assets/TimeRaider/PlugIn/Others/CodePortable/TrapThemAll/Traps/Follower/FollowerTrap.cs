﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

/**Represent  a trap that will detect a target and track it when detected in a trigger*/
public class FollowerTrap : TriggeredTrap {

	/** The  behaviour of the trap when the player is over him on the map.
	 *AllDirection: follow the player until death in all direction.
	 *RamLike: the trap dash on the player and continue forward.
	 *LemmingLike: the trap dash on the player and then stop moving.
	 */
	public enum FollowingType { AllDirection, RamLike, LemmingLike}

	/*Moving speed value  of the trap towards the target*/
	public float speed=0.2f;

	/*What does the trap is following */
	public GameObject target;

	/***/
	public FollowingType followingType = FollowingType.AllDirection;

	/* Define the detection radius distance of the trap**/
	public float detectionDistance;

	public new void Start(){
		base.Start ();
	
		
		
	}


	public void Update()
	{

		if(target!=null)
		{
			float ratioVitesse = speed*Time.deltaTime;
			switch(followingType)
			{

			case FollowingType.AllDirection: 
				this.transform.position-= (this.transform.position-target.transform.position)*ratioVitesse;
				break;

				
			case FollowingType.RamLike: 
				this.transform.position += Vector3.left*ratioVitesse;
				break;
				
			case FollowingType.LemmingLike: 
				if(target.transform.position.x< this.transform.position.x)
					this.transform.position += Vector3.left*ratioVitesse;
				break;


			}

		}
	}
	
	
	
	protected   override void OnTrapActivate()
	{
		base.OnTrapActivate ();
		block.SetActive (true);

		target = GameObject.FindWithTag("Player");
		
		
	}
	public override void OnCollisionEnterDectected(Collision2D col)
	{
		
		if (col.gameObject.tag.Equals ("Player")) 
		{
			ApplyEffectTo(col.gameObject);
			
		}
	}
	public override void OnCollisionExitDectected(Collision2D col)
	{
		
	
	}

	public override void OnCollisionDectected(Collision2D col)
	{
		
	}


}
