﻿using UnityEngine;
using System.Collections;

public class SpawningTrap : TriggeredTrap {

	public GameObject producted;
	public Transform atOfSpwaner;
	public float spawningDelay=2f;
	private float lastTimeSpawn;
	private float lastTimeCheck;


	
	public new void Start()
	{
		base.Start();
		if (atOfSpwaner.position == Vector3.zero && !NoWarning)
			Debug.LogWarning ("The spwaner point is not define");
		if (producted == null) {
			if(!NoWarning)
			Debug.LogWarning ("The spwaner has nothing to spawned");
			throw new UnityException("The spwaner has nothing to spawned");
		}
		
		
	}

	public void Update()
	{
		if (ActivatedTrap) {
			lastTimeCheck = Time.timeSinceLevelLoad;
			float timePast = Time.timeSinceLevelLoad - lastTimeSpawn;
			if (timePast > spawningDelay) {

				CreateInstanceOfTheTrapObject ();


				lastTimeSpawn = lastTimeCheck;
			}
		}

	}

	
	public void CreateInstanceOfTheTrapObject()
	{
		if (producted != null && ActivatedTrap && lastTimeCheck - lastTimeSpawn > spawningDelay) {
			
			lastTimeSpawn= Time.timeSinceLevelLoad;	
			GameObject.Instantiate (producted, atOfSpwaner.position, Quaternion.Euler (Vector3.zero));
		}
	}

	protected override void OnTrapReveal()
	{
		base.OnTrapReveal();
		
		
		
	}
	

	protected override void OnTrapActivate()
	{
		base.OnTrapActivate();
		
		CreateInstanceOfTheTrapObject ();
	}
	

}
