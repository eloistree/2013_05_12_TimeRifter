﻿using UnityEngine;
using System.Collections;

public class DEL_Destoy : MonoBehaviour {

	public void OnCollisionEnter2D(Collision2D col)
	{
		Destroy(col.gameObject);
		if(col.gameObject.tag.Equals("Player"))
		DEL_QuickReload.RestartScene();
	}
}
