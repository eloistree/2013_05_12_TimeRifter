﻿using UnityEngine;
using System.Collections;

public class DEL_JumpPlayer : MonoBehaviour {

	public float lastJump;
	public float delay =2f;
	public float force=1000f;
	void Update () {

		float time = Time.timeSinceLevelLoad - lastJump;
		if ( time>delay && Input.GetKey(KeyCode.Space)) {
		
			this.gameObject.transform.rigidbody2D.AddForce(Vector2.up*force );
			lastJump = Time.timeSinceLevelLoad ;
		}
	


	}
}
