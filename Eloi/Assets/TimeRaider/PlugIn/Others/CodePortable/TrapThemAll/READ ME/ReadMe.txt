/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */


"Trap Them All" is asset produced by Strée Eloi.
Under a company names Madmersion.

This asset is build to help developer to produce easily and quickly some trap for unity.

How to use:

1. - Use a prefab that correspond the most at what you want to build.
2. - Adapte it to what you realy want
	-> Change the sprite  (do not forgot the scrit associate to it)
	-> Set the trap on automatic if you want it to run during all the game
		Else,  add to the object a trap activator that will activate the trap when the "Player" will be detected
	-NB> You can give several type of detection
3. - You can warn the player on where is the trap by using a RevealActivator.
	It will make appear for x seconds the object linked to it
4. - Make your player inherit of 
	-> TrapEffectSensitif : it will provide all the effect that are associated to the trap
		when the player will trigger it
	-> DetectionTypeInterface: it will provide the trap, the ability of know
		if it can be triggered (depending on Sound, Visibility or/and Weight)
		
		
