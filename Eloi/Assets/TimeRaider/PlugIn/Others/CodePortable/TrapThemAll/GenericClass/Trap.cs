﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

public abstract class Trap : MonoBehaviour {
	/**
	 * Kill : kill the object
	 * Dommage: dommage the object
	 * Slow : slow the object
	 * Stun: stun the object
	 * Fear: make the object moving and incontrolable
	 * PushOver: push strongly the player in a direction
	 * PlagueyTalk: the object is discussing with something
	 * QuickSand: the object move is reduce and drag down during x secondes 
	 */
	public enum TrapEffect
	{Kill, Dommage, Slow, Stun,Fear,PushOver, PlagueyTalk , Quicksand, SpeedUp}


	/**root represent the big father gameobjet of the trap,
	 * in aim for exemple to the developper to be able
	 * to activate, deativate or Destroy the trap completely*/
	public GameObject root;

	/**Define the effect to apply with this kind of trap*/
	public TrapEffect [] trapEffect =  {TrapEffect.Kill} ;
	
	/**The trap will activate x seconds after the trigger has been detected a movement*/
	public float activationDelay;
	/**The trap will apply the effect of the trap x seconds after a collision has been detected */
	public float applicationEffectDelay;
	/**Time between the possibility to reapply the effect of the trap*/
	public float delayBetweenApply=1f;
	

	/**refert to the last time the effect have been apply*/
	protected float lastTimeApply;
	[HideInInspector]
	/**Define if the trap has been activated */
	public bool ActivatedTrap;	
	[HideInInspector]
	/**Define when the trap has been activated */
	public float ActivatedTrapTime;


	/**If autostart is true, the trap will be activated at the Start() methode. Can be used for */
	public bool AutoStart ;

	/** How many time the trap can be use (=can affect entities)*/
	public enum UseType
		{AllTime, Several}

	public UseType useType = UseType.AllTime; 

	/**if several time activated, use number going to be used to determinate if the trap is totaly used*/
	public int useNumber=3;

	/**If you know what you are doing and do not want warning for the object*/
	public bool NoWarning;

	public bool IsTrapHasBeenActivated(){
		return ActivatedTrap; }

	public void Awake(){
		if (root == null)
			root = this.gameObject;
	}
	public void Start()
	{
		if(AutoStart){ ActivateTrap();}
	}


	/**Display something in the game to warn the player of the trap*/
	public void RevealeTrap()
	{ 
		OnTrapReveal ();
	}

	/**Activate the trap in the game */
	public void ActivateTrap()
	{
		ActivatedTrapTime= Time.timeSinceLevelLoad;
		if(activationDelay==0)
		OnTrapActivate ();
		else Invoke("OnTrapActivate",activationDelay);
	}



	/**Apply the trap effect to this object if it is a trap sensitif*/
	protected  virtual void ApplyEffectTo(GameObject objToApply)
	{
		TrapEffectSensitif sensitifObjToApply = objToApply.GetComponent(typeof(TrapEffectSensitif)) as TrapEffectSensitif;
		if (sensitifObjToApply != null)
			ApplyEffectTo (sensitifObjToApply, objToApply);
	}
	/**Apply the trap effect to this object if it is a trap sensitif*/
	protected  void ApplyEffectTo(TrapEffectSensitif sensitifObjToApply, GameObject objToApply)
	{
		float time = Time.timeSinceLevelLoad;
		if(   time - lastTimeApply > delayBetweenApply   ){
			bool canItApply = true;
			if(! UseType.AllTime.Equals (useType))
			{
				if(UseType.Several.Equals (useType) && useNumber<=0)
				{
					canItApply=false;
				}
			}

			if (canItApply) { 
					if (applicationEffectDelay == 0) {
							useNumber--;
							sensitifObjToApply.ApplyEffect (trapEffect, root);
					} else if (tmpApplyEffect == null) {
		
							tmpApplyEffect = new CoroutineApplyEffectParams (sensitifObjToApply, trapEffect, root);
							Invoke ("ApplyEffectToLater", applicationEffectDelay);
					}
				lastTimeApply=time;
				}
		}
	}

	private CoroutineApplyEffectParams tmpApplyEffect;
	protected  void ApplyEffectToLater()
	{
		useNumber--;
		tmpApplyEffect.objToApply.ApplyEffect (tmpApplyEffect.effect, tmpApplyEffect.trapRoot);
		tmpApplyEffect = null;

	}
	/**Bad code #### Coroutine take only one arguement so I stock it in this 
	 *tmpvariable instead of creating a classe with them*/
	public class CoroutineApplyEffectParams{

		public CoroutineApplyEffectParams(  TrapEffectSensitif objToApply, TrapEffect [] effect, GameObject trapRoot)
		{
			if( effect==null || trapRoot==null  || objToApply==null)
			{
				throw new UnityException("Null pointer exception");
			} 
			this.effect = effect;
			this.trapRoot = trapRoot;
			this.objToApply = objToApply;
		}
	
		public TrapEffect [] effect;
		public GameObject trapRoot;
		public TrapEffectSensitif objToApply;
	
		public override string ToString ()
		{
			return string.Format ("[CoroutineApplyEffectParams:"+objToApply +" "+ effect +" "+trapRoot+" ]");
		}
		
	}
	
	/**Give the children of the classe the ability 
	 * to define what will happen when the game design
	 * want to reveal the location of the trap */
	protected virtual  void OnTrapReveal(){}
	
	/**Give the children of the classe the ability 
	 * to define what will happen when the game design
	 * want to activate the trap */
	protected virtual  void OnTrapActivate(){ ActivatedTrap = true; }



	/**What do the trap have to do if a collision is detected on a child gameobject that is listening to collision*/
	public virtual void OnCollisionEnterDectected(Collision2D col){}
	/**What do the trap have to do if a collision is detected on a child gameobject that is listening to collision*/
	public  virtual void OnCollisionExitDectected(Collision2D col){}
	/**What do the trap have to do if a collision is detected on a child gameobject that is listening to collision*/
	public virtual  void OnCollisionDectected(Collision2D col){}

	/**What do the trap have to do if a collision is detected on a child gameobject that is listening to collision*/
	public virtual void OnCollisionEnterDectected(Collider2D col){}
	/**What do the trap have to do if a collision is detected on a child gameobject that is listening to collision*/
	public  virtual void OnCollisionExitDectected(Collider2D col){}
	/**What do the trap have to do if a collision is detected on a child gameobject that is listening to collision*/
	public virtual  void OnCollisionDectected(Collider2D col){}




}



/**
public interface TrapHittable
{
	 void OnTrapCollisionEnter (Collision2D col,TrapCollisionType colType);
	 void OnTrapCollision (Collision2D col);

}
public enum TrapCollisionType {Deathly, Hurting,Slow, Innocuous}
*/