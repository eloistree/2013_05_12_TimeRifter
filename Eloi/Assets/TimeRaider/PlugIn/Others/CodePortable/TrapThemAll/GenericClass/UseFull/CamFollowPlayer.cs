﻿using UnityEngine;
using System.Collections;

public class CamFollowPlayer : MonoBehaviour {

	public Camera cam ;
	public GameObject player;
	
	public float PourcentRight=80f;
	public bool withSmoothMove=true;
	public float smoothVelocity =0.12f;




	void Start () {
		if (cam == null)
		cam = Camera.main;
		camPositionBefront.z = cam.transform.position.z;
		if (player == null)
		player = GameObject.FindWithTag ("Player");
	}


	void Update () {
	
		if (cam != null && player!=null) {
			camPositionBefront.x = player.transform.position.x+(cam.orthographicSize*PourcentRight/40f);
			camPositionBefront.y = player.transform.position.y;



			cam.transform.position += (camPositionBefront-cam.transform.position )*smoothVelocity;
		}
	}


	private Vector3 camPositionBefront=new Vector3();
	private float moveCamRight;

}
