/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * streeeloi@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 * Strée Eloi
 * 
 * Contact: http://www.stree.be/eloi
 * ----------------------------------------------------------------------------
 */


using UnityEngine;
using System.Collections;

public abstract class TriggeredTrap : Trap
{
public GameObject revealer;
public GameObject block;

	public float timeRevealed=1.5f;


public new void Start()
{
		base.Start();
		if (revealer == null && !NoWarning)
		Debug.LogWarning ("The trap have not revealer");
		if (block == null  ) {
			if(!NoWarning)
			Debug.LogWarning ("The trap is not define");
			throw new UnityException("The trap is not define");
	}
	
	
}


protected override void OnTrapReveal()
{
		base.OnTrapReveal();
		StartCoroutine ("RevealTrapFor",timeRevealed);
	
	
	
}

protected IEnumerator RevealTrapFor(float time){
		if(revealer!=null){
			revealer.SetActive (true);
			yield return new WaitForSeconds (time);
			revealer.SetActive (false);
		}
	StopCoroutine ("RevealTrapFor");
	
}
	protected override void OnTrapActivate()
	{
		base.OnTrapActivate ();
	}

	



}